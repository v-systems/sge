<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>SGE</title>

    <!-- Fonts -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>

    <!-- Styles -->
    <link href="{{ asset('/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}

    <style>
        body {
            font-family: 'Lato';
        }

        .fa-btn {
            margin-right: 6px;
        }

        .container {
            width: auto;
            max-width: 1000px;
            padding: 0 15px;
        }
        .container .text-muted {
            margin: 20px 0;
        }
    </style>
</head>
<body id="app-layout">
    <div class="container" style="margin-top: 50px;">
        <div class="col-md-4 col-md-offset-4">
            <h1 class="cover-heading" style="font-size: 53px; margin-top: 150px;">SGE</h1>
            <a href="{!! url('/admin') !!}" class="btn btn-primary">Administrativo</a>
        </div>
    </div>

    <!-- JavaScripts -->
    <script src="{{ asset('/plugins/jQuery/jQuery-2.1.4.min.js') }}"></script>
    <script src="{{ asset('/bootstrap/js/bootstrap.min.js') }}"></script>
    {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}
</body>
</html>
