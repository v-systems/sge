{!! Form::hidden('id', null) !!}
<div class="box-body">

    <div class="form-group {!! $errors->has('role_id') ? 'has-error' : '' !!}">
        {!! Form::label('role_id', 'Perfil', ['class' => 'control-label col-md-2']) !!}
        <div class="col-md-10">
            {!! Form::select('role_id', $roles, old('role_id'), ['class' => 'form-control']) !!}
            @if ($errors->has('role_id'))
                <span class="help-block">
                    <strong>{{ $errors->first('role_id') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <div class="form-group {!! $errors->has('enterprise_id') ? 'has-error' : '' !!}">
        {!! Form::label('enterprise_id', 'Empresa', ['class' => 'control-label col-md-2']) !!}
        <div class="col-md-10">
            {!! Form::select('enterprise_id', $enterprises, old('enterprise_id'), ['class' => 'form-control']) !!}
            @if ($errors->has('enterprise_id'))
                <span class="help-block">
                    <strong>{{ $errors->first('enterprise_id') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <div class="form-group {!! $errors->has('name') ? 'has-error' : '' !!}">
        {!! Form::label('name', 'Nome', ['class' => 'control-label col-md-2']) !!}
        <div class="col-md-10">
            {!! Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => 'Nome']) !!}
            @if ($errors->has('name'))
                <span class="help-block">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
            @endif
        </div>
    </div>

	<div class="form-group {!! $errors->has('username') ? 'has-error' : '' !!}">
		{!! Form::label('username', 'Nome', ['class' => 'control-label col-md-2']) !!}
		<div class="col-md-10">
			{!! Form::text('username', old('username'), ['class' => 'form-control', 'placeholder' => 'Nome de usuário']) !!}
            @if ($errors->has('username'))
                <span class="help-block">
                    <strong>{{ $errors->first('username') }}</strong>
                </span>
            @endif
		</div>
	</div>

	<div class="form-group {!! $errors->has('email') ? 'has-error' : '' !!}">
		{!! Form::label('email', 'E-mail', ['class' => 'control-label col-md-2']) !!}
		<div class="col-md-10">
			{!! Form::text('email', old('email'), ['class' => 'form-control', 'placeholder' => 'E-mail']) !!}
            @if ($errors->has('email'))
                <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif
		</div>
	</div>

	<div class="form-group {!! $errors->has('password') ? 'has-error' : '' !!}">
		{!! Form::label('password', 'Senha', ['class' => 'control-label col-md-2']) !!}
		<div class="col-md-10">
			{!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'Senha']) !!}
            @if ($errors->has('password'))
                <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
            @endif
		</div>
	</div>

	<div class="form-group {!! $errors->has('password_confirmation') ? 'has-error' : '' !!}">
		{!! Form::label('password_confirmation', 'Confirmação de Senha', ['class' => 'control-label col-md-2']) !!}
		<div class="col-md-10">
			{!! Form::password('password_confirmation', ['class' => 'form-control', 'placeholder' => 'Confirmação de Senha']) !!}
            @if ($errors->has('password_confirmation'))
                <span class="help-block">
                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                </span>
            @endif
		</div>
	</div>

</div>

<div class="box-footer">
    <a href="{!! route('users.index') !!}" class="btn btn-default">Voltar</a>
    <button type="submit" class="btn btn-info pull-right">Salvar</button>
</div>
