@extends('layouts.app')

@section('content-header', 'Usuários')

@section('content')
	<div class="row">
		<div class="col-md-12">
			<div class="box">
				<div class="box-header with-border">
					<a type="button" class="btn btn-primary" href="{{ route('users.create') }}"><i class="fa fa-plus"></i> Adicionar</a>
					<div class="clearfix">&nbsp;</div>
					<table class="table table-bordered table-striped" id="dataTableList">
						<thead>
							<tr>
								<td>Empresa</td>
                                <td>Nome</td>
                                <td>Nome de Usuário</td>
								<td>Perfil</td>
								<td>E-mail</td>
								<td class="col-md-1">Ações</td>
							</tr>
						</thead>

						<tbody>
							@foreach ($users as $user)
							    <tr>
                                    <td>{{ $user->enterprise->name }}</td>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->username }}</td>
                                    <td>{{ $user->role->label }}</td>
							    	<td>{{ $user->email }}</td>
							    	<td nowrap="nowrap">
							    		{{ Form::open(['route' => ['users.destroy', $user->id], 'method'=> 'delete', 'class' => 'form-inline']) }}
							    			{!! Form::hidden('id', $user->id) !!}
	                                        <a type="button" class="btn btn-primary tooltips" data-toggle="tooltip" data-placement="top" title="Editar" href="{{ route('users.edit', $user->id) }}"><i class="fa fa-edit"></i></a>
											<button type="button" class="btn btn-danger tooltips" data-toggle="modal" data-target="#modalDelete-{{ $user->id }}" title="Remover">
									            <i class="fa fa-trash-o"></i>
									        </button>
									        <!-- Modal -->
									        <div class="modal fade" id="modalDelete-{{ $user->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
									             aria-hidden="true">
									            <div class="modal-dialog">
									                <div class="modal-content">
									                    <div class="modal-header">
									                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
									                        <h3 class="modal-title modal-title-delete">Atenção!</h3>
									                    </div>
									                    <div class="modal-body">
									                        Deseja realmente remover?
									                    </div>
									                    <div class="modal-footer">
									                        <button type="button" class="btn btn-default btn-modal-close" data-dismiss="modal">Voltar</button>
									                        <button type="submit" class="btn btn-danger">Remover</button>
									                    </div>
									                </div>
									            </div>
									        </div>
									    {{ Form::close() }}
							    	</td>
							    </tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
@endsection
