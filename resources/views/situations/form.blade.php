{!! Form::hidden('id', null) !!}
<div class="box-body">

	<div class="form-group {!! $errors->has('name') ? 'has-error' : '' !!}">
		{!! Form::label('name', 'Nome', ['class' => 'control-label col-md-2']) !!}
		<div class="col-md-10">
			{!! Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => 'Nome']) !!}
            @if ($errors->has('name'))
                <span class="help-block">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
            @endif
		</div>
	</div>
    <div class="form-group {!! $errors->has('description') ? 'has-error' : '' !!}">
        {!! Form::label('label', 'Descrição', ['class' => 'control-label col-md-2']) !!}
        <div class="col-md-10">
            {!! Form::text('description', old('description'), ['class' => 'form-control', 'placeholder' => 'Descrição']) !!}
            @if ($errors->has('description'))
                <span class="help-block">
                    <strong>{{ $errors->first('description') }}</strong>
                </span>
            @endif
        </div>
    </div>
</div>

<div class="box-footer">
    <a href="{!! route('situations.index') !!}" class="btn btn-default">Voltar</a>
    <button type="submit" class="btn btn-info pull-right">Salvar</button>
</div>
