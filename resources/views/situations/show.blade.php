@extends('layouts.app')

@section('content-header', 'Situação')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Visualizar</h3>
            </div>

            <div class="box-body form-horizontal">
                <div class="form-group">
                    {!! Form::label('name', 'Nome', ['class' => 'control-label col-md-2']) !!}
                    <div class="col-md-10">
                        <p class="form-control-static">{!! $situation->name !!}</p>
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('description', 'Descrição', ['class' => 'control-label col-md-2']) !!}
                    <div class="col-md-10">
                        <p class="form-control-static">{!! $situation->description !!}</p>
                    </div>
                </div>
            </div>

            <div class="box-footer">
                <a href="{!! route('situations.index') !!}" class="btn btn-default">Voltar</a>
            </div>
        </div>
    </div>
</div>
@endsection
