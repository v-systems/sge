@extends('layouts.app')

@section('content-header', 'Números e Chips')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Editar</h3>
			</div>
			{!! Form::model($chipNumber, ['route' => ['chipNumbers.update', $chipNumber->id], 'method' => 'PATCH', 'class' => 'form-horizontal', 'role' => 'form']) !!}
                @include('chipNumbers.form', [$chipNumber])
			{!! Form::close() !!}
		</div>
	</div>
</div>
@endsection
