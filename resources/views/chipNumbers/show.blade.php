@extends('layouts.app')

@section('content-header', 'Números e Chips')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Visualizar</h3>
            </div>

            <div class="box-body form-horizontal">
                <div class="form-group">
                    {!! Form::label('number', 'Número', ['class' => 'control-label col-md-2']) !!}
                    <div class="col-md-10">
                        <p class="form-control-static">{!! $chipNumber->number !!}</p>
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('chip_number', 'Número do Chip', ['class' => 'control-label col-md-2']) !!}
                    <div class="col-md-10">
                        <p class="form-control-static">{!! $chipNumber->chip_number !!}</p>
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('cellphone_plan_id', 'Plano', ['class' => 'control-label col-md-2']) !!}
                    <div class="col-md-10">
                        <p class="form-control-static">{!! $chipNumber->cellphonePlan->name !!}</p>
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('data', 'Dados', ['class' => 'control-label col-md-2']) !!}
                    <div class="col-md-10">
                        <p class="form-control-static">{!! $chipNumber->data !!}</p>
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('buy_date', 'Data da Compra', ['class' => 'control-label col-md-2']) !!}
                    <div class="col-md-10">
                        <p class="form-control-static">{!! $chipNumber->buy_date->format('d/m/Y') !!}</p>
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('invoice_number', 'Número da NF', ['class' => 'control-label col-md-2']) !!}
                    <div class="col-md-10">
                        <p class="form-control-static">{!! !empty($chipNumber->invoice_number) ? $chipNumber->invoice_number : '-' !!}</p>
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('employee', 'Funcionário', ['class' => 'control-label col-md-2']) !!}
                    <div class="col-md-10">
                        <p class="form-control-static">{!! $chipNumber->employee !!}</p>
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('job_id', 'Função', ['class' => 'control-label col-md-2']) !!}
                    <div class="col-md-10">
                        <p class="form-control-static">{!! $chipNumber->job->name !!}</p>
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('possession_date', 'Data da Entrega', ['class' => 'control-label col-md-2']) !!}
                    <div class="col-md-10">
                        <p class="form-control-static">{!! $chipNumber->possession_date->format('d/m/Y') !!}</p>
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('line_situation_id', 'Situação', ['class' => 'control-label col-md-2']) !!}
                    <div class="col-md-10">
                        <p class="form-control-static">{!! $chipNumber->lineSituation->name !!}</p>
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('historic', 'Histórico', ['class' => 'control-label col-md-2']) !!}
                    <div class="col-md-10">
                        <p class="form-control-static">{!! !empty($chipNumber->historic) ? nl2br($chipNumber->historic) : '-' !!}</p>
                    </div>
                </div>
            </div>

            <div class="box-footer">
                <a href="{!! route('chipNumbers.index') !!}" class="btn btn-default">Voltar</a>
            </div>
        </div>
    </div>
</div>
@endsection
