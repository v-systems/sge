{!! Form::hidden('id', null) !!}
<div class="box-body">
    @include('partials.enterprise_field')

	<div class="form-group {!! $errors->has('number') ? 'has-error' : '' !!}">
		{!! Form::label('number', 'Número', ['class' => 'control-label col-md-2']) !!}
		<div class="col-md-10">
			{!! Form::number('number', old('number'), ['class' => 'form-control', 'maxlength' => '12']) !!}
			@if ($errors->has('number'))
	            <span class="help-block">
	                <strong>{{ $errors->first('number') }}</strong>
	            </span>
	        @endif
		</div>
	</div>

	<div class="form-group {!! $errors->has('chip_number') ? 'has-error' : '' !!}">
		{!! Form::label('chip_number', 'Número do Chip', ['class' => 'control-label col-md-2']) !!}
		<div class="col-md-10">
			{!! Form::number('chip_number', old('chip_number'), ['class' => 'form-control', 'maxlength' => '12']) !!}
			@if ($errors->has('chip_number'))
	            <span class="help-block">
	                <strong>{{ $errors->first('chip_number') }}</strong>
	            </span>
	        @endif
		</div>
	</div>

	<div class="form-group {!! $errors->has('cellphone_plan_id') ? 'has-error' : '' !!}">
		{!! Form::label('cellphone_plan_id', 'Plano', ['class' => 'control-label col-md-2']) !!}
		<div class="col-md-10">
			{!! Form::select('cellphone_plan_id', $cellphonePlans, old('cellphone_plan_id'), ['class' => 'form-control']) !!}
			@if ($errors->has('cellphone_plan_id'))
	            <span class="help-block">
	                <strong>{{ $errors->first('cellphone_plan_id') }}</strong>
	            </span>
	        @endif
		</div>
	</div>

	<div class="form-group {!! $errors->has('data') ? 'has-error' : '' !!}">
		{!! Form::label('data', 'Dados', ['class' => 'control-label col-md-2']) !!}
		<div class="col-md-10">
			{!! Form::select('data', ['Sim' => 'Sim', 'Não' => 'Não'], old('data'), ['class' => 'form-control']) !!}
			@if ($errors->has('data'))
	            <span class="help-block">
	                <strong>{{ $errors->first('data') }}</strong>
	            </span>
	        @endif
		</div>
	</div>

	<div class="form-group {!! $errors->has('buy_date') ? 'has-error' : '' !!}">
		{!! Form::label('buy_date', 'Data da Compra', ['class' => 'control-label col-md-2']) !!}
		<div class="col-md-10">
			{!! Form::text('buy_date', !empty($chipNumber->buy_date) ? $chipNumber->buy_date->format('d/m/Y') : old('buy_date'), ['class' => 'form-control datepicker']) !!}
			@if ($errors->has('buy_date'))
	            <span class="help-block">
	                <strong>{{ $errors->first('buy_date') }}</strong>
	            </span>
	        @endif
		</div>
	</div>

	<div class="form-group {!! $errors->has('invoice_number') ? 'has-error' : '' !!}">
		{!! Form::label('invoice_number', 'Número da NF', ['class' => 'control-label col-md-2']) !!}
		<div class="col-md-10">
			{!! Form::number('invoice_number', old('invoice_number'), ['class' => 'form-control']) !!}
			@if ($errors->has('invoice_number'))
	            <span class="help-block">
	                <strong>{{ $errors->first('invoice_number') }}</strong>
	            </span>
	        @endif
		</div>
	</div>

	<div class="form-group {!! $errors->has('employee') ? 'has-error' : '' !!}">
		{!! Form::label('employee', 'Funcionário', ['class' => 'control-label col-md-2']) !!}
		<div class="col-md-10">
			{!! Form::text('employee', old('employee'), ['class' => 'form-control']) !!}
			@if ($errors->has('employee'))
	            <span class="help-block">
	                <strong>{{ $errors->first('employee') }}</strong>
	            </span>
	        @endif
		</div>
	</div>

	<div class="form-group {!! $errors->has('job_id') ? 'has-error' : '' !!}">
		{!! Form::label('job_id', 'Função', ['class' => 'control-label col-md-2']) !!}
		<div class="col-md-10">
			{!! Form::select('job_id', $jobs, old('job_id'), ['class' => 'form-control']) !!}
			@if ($errors->has('job_id'))
	            <span class="help-block">
	                <strong>{{ $errors->first('job_id') }}</strong>
	            </span>
	        @endif
		</div>
	</div>

	<div class="form-group {!! $errors->has('possession_date') ? 'has-error' : '' !!}">
		{!! Form::label('possession_date', 'Data da Entrega', ['class' => 'control-label col-md-2']) !!}
		<div class="col-md-10">
			{!! Form::text('possession_date', !empty($chipNumber->possession_date) ? $chipNumber->possession_date->format('d/m/Y') : old('possession_date'), ['class' => 'form-control datepicker']) !!}
			@if ($errors->has('possession_date'))
	            <span class="help-block">
	                <strong>{{ $errors->first('possession_date') }}</strong>
	            </span>
	        @endif
		</div>
	</div>

	<div class="form-group {!! $errors->has('line_situation_id') ? 'has-error' : '' !!}">
		{!! Form::label('line_situation_id', 'Situação', ['class' => 'control-label col-md-2']) !!}
		<div class="col-md-10">
			{!! Form::select('line_situation_id', $lineSituations, old('line_situation_id'), ['class' => 'form-control']) !!}
			@if ($errors->has('line_situation_id'))
	            <span class="help-block">
	                <strong>{{ $errors->first('line_situation_id') }}</strong>
	            </span>
	        @endif
		</div>
	</div>

	<div class="form-group {!! $errors->has('historic') ? 'has-error' : '' !!}">
		{!! Form::label('historic', 'Histórico', ['class' => 'control-label col-md-2']) !!}
		<div class="col-md-10">
			{!! Form::textarea('historic', !empty($chipNumber->historic) ? $chipNumber->historic : old('historic'), ['class' => 'form-control']) !!}
			@if ($errors->has('historic'))
	            <span class="help-block">
	                <strong>{{ $errors->first('historic') }}</strong>
	            </span>
	        @endif
		</div>
	</div>
</div>

<div class="box-footer">
    <a href="{!! route('chipNumbers.index') !!}" class="btn btn-default">Voltar</a>
    <button type="submit" class="btn btn-info pull-right">Salvar</button>
</div>
