@extends('layouts.app')

@section('content-header', 'Números e Chips')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <a type="button" class="btn btn-primary" href="{{ route('chipNumbers.create') }}"><i class="fa fa-plus"></i> Adicionar</a>
                    <div class="clearfix">&nbsp;</div>
                    <table class="table table-bordered table-striped" id="dataTableList">
                        <thead>
                            <tr>
                                @if (Auth::user()->role_id == 1)
                                    <td>Empresa</td>
                                @endif
                                <td>Número</td>
                                <td>Funcionário</td>
                                <td>Número do Chip</td>
                                <td>Plano</td>
                                <td>Dados</td>
                                <td>Data Compra</td>
                                <td class="col-md-1">Ações</td>
                            </tr>
                        </thead>

                        <tbody>
                            @foreach ($chipNumbers as $chipNumber)
                                <tr>
                                    @if (Auth::user()->role_id == 1)
                                        <td>{{ $chipNumber->enterprise->name }}</td>
                                    @endif
                                    <td>{{ $chipNumber->number }}</td>
                                    <td>{{ $chipNumber->employee }}</td>
                                    <td>{{ $chipNumber->chip_number }}</td>
                                    <td>{{ $chipNumber->cellphonePlan->name }}</td>
                                    <td>{{ $chipNumber->data }}</td>
                                    <td>{{ $chipNumber->buy_date->format('d/m/Y') }}</td>
                                    <td nowrap="nowrap">
                                        {{ Form::open(['route' => ['chipNumbers.destroy', $chipNumber->id], 'method'=> 'delete', 'class' => 'form-inline']) }}
                                            @include('partials.actions', ['object' => $chipNumber, 'route' => 'chipNumbers'])
                                        {{ Form::close() }}
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
