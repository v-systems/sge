@extends('layouts.app')

@section('content-header', 'Funcionários')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <a type="button" class="btn btn-primary" href="{{ route('employees.create') }}"><i class="fa fa-plus"></i> Adicionar</a>
                    <div class="clearfix">&nbsp;</div>
                    <table class="table table-bordered table-striped" id="dataTableList">
                        <thead>
                            <tr>
                                <td>Nome</td>
                                <td>Empresa</td>
                                <td>Departamento</td>
                                <td class="col-md-1">Ações</td>
                            </tr>
                        </thead>

                        <tbody>
                            @foreach ($employees as $employee)
                                <tr>
                                    <td>{{ $employee->name }}</td>
                                    <td>{{ $employee->enterprise->name }}</td>
                                    <td>{{ $employee->department->name }}</td>
                                    <td nowrap="nowrap">
                                        {{ Form::open(['route' => ['employees.destroy', $employee->id], 'method'=> 'delete', 'class' => 'form-inline']) }}
                                            @include('partials.actions', ['object' => $employee, 'route' => 'employees'])
                                        {{ Form::close() }}
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
