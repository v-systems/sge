{!! Form::hidden('id', null) !!}
<div class="box-body">
    @include('partials.enterprise_field')

	<div class="form-group {!! $errors->has('department_id') ? 'has-error' : '' !!}">
		{!! Form::label('department_id', 'Departamento', ['class' => 'control-label col-md-2']) !!}
		<div class="col-md-10">
			{!! Form::select('department_id', $departments, old('department_id'), ['class' => 'form-control']) !!}
			@if ($errors->has('department_id'))
	            <span class="help-block">
	                <strong>{{ $errors->first('department_id') }}</strong>
	            </span>
	        @endif
		</div>
	</div>

	<div class="form-group {!! $errors->has('name') ? 'has-error' : '' !!}">
		{!! Form::label('name', 'Nome', ['class' => 'control-label col-md-2']) !!}
		<div class="col-md-10">
			{!! Form::text('name', old('name'), ['class' => 'form-control']) !!}
			@if ($errors->has('name'))
	            <span class="help-block">
	                <strong>{{ $errors->first('name') }}</strong>
	            </span>
	        @endif
		</div>
	</div>
</div>

<div class="box-footer">
    <a href="{!! route('employees.index') !!}" class="btn btn-default">Voltar</a>
    <button type="submit" class="btn btn-info pull-right">Salvar</button>
</div>
