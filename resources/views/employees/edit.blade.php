@extends('layouts.app')

@section('content-header', 'Funcionários')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Editar</h3>
			</div>
			{!! Form::model($employee, ['route' => ['employees.update', $employee->id], 'method' => 'PATCH', 'class' => 'form-horizontal', 'role' => 'form']) !!}
                @include('employees.form', [$employee])
			{!! Form::close() !!}
		</div>
	</div>
</div>
@endsection
