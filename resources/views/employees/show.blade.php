@extends('layouts.app')

@section('content-header', 'Funcionários')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Visualizar</h3>
            </div>

            <div class="box-body form-horizontal">
                <div class="form-group">
                    {!! Form::label('enterprise', 'Empresa', ['class' => 'control-label col-md-2']) !!}
                    <div class="col-md-10">
                        <p class="form-control-static">{!! $employee->enterprise->name !!}</p>
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('department', 'Departamento', ['class' => 'control-label col-md-2']) !!}
                    <div class="col-md-10">
                        <p class="form-control-static">{!! $employee->department->name !!}</p>
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('name', 'Nome', ['class' => 'control-label col-md-2']) !!}
                    <div class="col-md-10">
                        <p class="form-control-static">{!! $employee->name !!}</p>
                    </div>
                </div>
            </div>

            <div class="box-footer">
                <a href="{!! route('employees.index') !!}" class="btn btn-default">Voltar</a>
            </div>
        </div>
    </div>
</div>
@endsection
