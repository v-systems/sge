@extends('layouts.app')

@section('content-header', 'Local Instalado')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <a type="button" class="btn btn-primary" href="{{ route('locals.create') }}"><i class="fa fa-plus"></i> Adicionar</a>
                    <div class="clearfix">&nbsp;</div>
                    <table class="table table-bordered table-striped" id="dataTableList">
                        <thead>
                            <tr>
                                <td>Nome</td>
                                <td>Descrição</td>
                                <td class="col-md-1">Ações</td>
                            </tr>
                        </thead>

                        <tbody>
                            @foreach ($locals as $local)
                                <tr>
                                    <td>{{ $local->name }}</td>
                                    <td>{{ $local->description }}</td>
                                    <td nowrap="nowrap">
                                        {{ Form::open(['route' => ['locals.destroy', $local->id], 'method'=> 'delete', 'class' => 'form-inline']) }}
                                            @include('partials.actions', ['object' => $local, 'route' => 'locals'])
                                        {{ Form::close() }}
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
