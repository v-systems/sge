@extends('layouts.app')

@section('content-header', 'Local Instalado')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Adicionar</h3>
			</div>
			{!! Form::open(['route' => ['locals.store'], 'class' => 'form-horizontal', 'role' => 'form']) !!}
				@include('locals.form')
			{!! Form::close() !!}
		</div>
	</div>
</div>
@endsection
