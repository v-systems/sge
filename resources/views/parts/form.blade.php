{!! Form::hidden('id', null) !!}
<div class="box-body">
    @include('partials.enterprise_field')

	<div class="form-group {!! $errors->has('code') ? 'has-error' : '' !!}">
		{!! Form::label('code', 'Código', ['class' => 'control-label col-md-2']) !!}
		<div class="col-md-10">
			{!! Form::number('code', old('code'), ['class' => 'form-control', 'maxlength' => '12']) !!}
			@if ($errors->has('code'))
	            <span class="help-block">
	                <strong>{{ $errors->first('code') }}</strong>
	            </span>
	        @endif
		</div>
	</div>

	<div class="form-group {!! $errors->has('equipment_id') ? 'has-error' : '' !!}">
		{!! Form::label('equipment_id', 'Equipamento', ['class' => 'control-label col-md-2']) !!}
		<div class="col-md-10">
			{!! Form::select('equipment_id', $equipments, old('equipment_id'), ['class' => 'form-control']) !!}
			@if ($errors->has('equipment_id'))
	            <span class="help-block">
	                <strong>{{ $errors->first('equipment_id') }}</strong>
	            </span>
	        @endif
		</div>
	</div>

	<div class="form-group {!! $errors->has('description') ? 'has-error' : '' !!}">
		{!! Form::label('description', 'Descrição', ['class' => 'control-label col-md-2']) !!}
		<div class="col-md-10">
			{!! Form::text('description', old('description'), ['class' => 'form-control']) !!}
			@if ($errors->has('description'))
	            <span class="help-block">
	                <strong>{{ $errors->first('description') }}</strong>
	            </span>
	        @endif
		</div>
	</div>

	<div class="form-group">
		{!! Form::label('serial_number', 'N.º Série', ['class' => 'control-label col-md-2']) !!}
		<div class="col-md-10">
			{!! Form::text('serial_number', old('serial_number'), ['class' => 'form-control']) !!}
		</div>
	</div>

	<div class="form-group">
		{!! Form::label('imei', 'IMEI', ['class' => 'control-label col-md-2']) !!}
		<div class="col-md-10">
			{!! Form::text('imei', old('imei'), ['class' => 'form-control']) !!}
		</div>
	</div>

	<div class="form-group {!! $errors->has('buy_date') ? 'has-error' : '' !!}">
		{!! Form::label('buy_date', 'Data da Compra', ['class' => 'control-label col-md-2']) !!}
		<div class="col-md-10">
			{!! Form::text('buy_date', !empty($part->buy_date) ? $part->buy_date->format('d/m/Y') : old('buy_date'), ['class' => 'form-control datepicker']) !!}
			@if ($errors->has('buy_date'))
	            <span class="help-block">
	                <strong>{{ $errors->first('buy_date') }}</strong>
	            </span>
	        @endif
		</div>
	</div>

	<div class="form-group {!! $errors->has('invoice_number') ? 'has-error' : '' !!}">
		{!! Form::label('invoice_number', 'Número da NF', ['class' => 'control-label col-md-2']) !!}
		<div class="col-md-10">
			{!! Form::text('invoice_number', old('invoice_number'), ['class' => 'form-control']) !!}
			@if ($errors->has('invoice_number'))
	            <span class="help-block">
	                <strong>{{ $errors->first('invoice_number') }}</strong>
	            </span>
	        @endif
		</div>
	</div>

	<div class="form-group">
		{!! Form::label('supplier', 'Fornecedor', ['class' => 'control-label col-md-2']) !!}
		<div class="col-md-10">
			{!! Form::text('supplier', old('supplier'), ['class' => 'form-control']) !!}
		</div>
	</div>

	<div class="form-group {!! $errors->has('local_id') ? 'has-error' : '' !!}">
		{!! Form::label('local_id', 'Local Instalado', ['class' => 'control-label col-md-2']) !!}
		<div class="col-md-10">
			{!! Form::select('local_id', $locals, old('local_id'), ['class' => 'form-control']) !!}
			@if ($errors->has('local_id'))
	            <span class="help-block">
	                <strong>{{ $errors->first('local_id') }}</strong>
	            </span>
	        @endif
		</div>
	</div>

	<div class="form-group {!! $errors->has('department_id') ? 'has-error' : '' !!}">
		{!! Form::label('department_id', 'Departamento', ['class' => 'control-label col-md-2']) !!}
		<div class="col-md-10">
			{!! Form::select('department_id', $departments, old('department_id'), ['class' => 'form-control']) !!}
			@if ($errors->has('department_id'))
	            <span class="help-block">
	                <strong>{{ $errors->first('department_id') }}</strong>
	            </span>
	        @endif
		</div>
	</div>

	<div class="form-group {!! $errors->has('computer_id') ? 'has-error' : '' !!}">
		{!! Form::label('computer_id', 'Comp./Local instalação', ['class' => 'control-label col-md-2']) !!}
		<div class="col-md-10">
			{!! Form::select('computer_id', $computers, old('computer_id'), ['class' => 'form-control']) !!}
			@if ($errors->has('computer_id'))
	            <span class="help-block">
	                <strong>{{ $errors->first('computer_id') }}</strong>
	            </span>
	        @endif
		</div>
	</div>

	<div class="form-group {!! $errors->has('situation_id') ? 'has-error' : '' !!}">
		{!! Form::label('situation_id', 'Situação', ['class' => 'control-label col-md-2']) !!}
		<div class="col-md-10">
			{!! Form::select('situation_id', $situations, old('situation_id'), ['class' => 'form-control']) !!}
			@if ($errors->has('situation_id'))
	            <span class="help-block">
	                <strong>{{ $errors->first('situation_id') }}</strong>
	            </span>
	        @endif
		</div>
	</div>

	<div class="form-group {!! $errors->has('installation_date') ? 'has-error' : '' !!}">
		{!! Form::label('installation_date', 'Data da Instalação', ['class' => 'control-label col-md-2']) !!}
		<div class="col-md-10">
			{!! Form::text('installation_date', !empty($part->installation_date) ? $part->installation_date->format('d/m/Y') : old('installation_date'), ['class' => 'form-control datepicker']) !!}
			@if ($errors->has('installation_date'))
	            <span class="help-block">
	                <strong>{{ $errors->first('installation_date') }}</strong>
	            </span>
	        @endif
		</div>
	</div>

	<div class="form-group {!! $errors->has('warranty') ? 'has-error' : '' !!}">
		{!! Form::label('warranty', 'Garantia', ['class' => 'control-label col-md-2']) !!}
		<div class="col-md-10">
			{!! Form::select('warranty', ['3 meses', '1 ano', '3 anos'], old('warranty'), ['class' => 'form-control']) !!}
			@if ($errors->has('warranty'))
	            <span class="help-block">
	                <strong>{{ $errors->first('warranty') }}</strong>
	            </span>
	        @endif
		</div>
	</div>

    <div class="form-group {!! $errors->has('historic') ? 'has-error' : '' !!}">
        {!! Form::label('historic', 'Histórico', ['class' => 'control-label col-md-2']) !!}
        <div class="col-md-10">
            {!! Form::textarea('historic', !empty($part->historic) ? $part->historic : old('historic'), ['class' => 'form-control']) !!}
            @if ($errors->has('historic'))
                <span class="help-block">
                    <strong>{{ $errors->first('historic') }}</strong>
                </span>
            @endif
        </div>
    </div>
</div>

<div class="box-footer">
    <a href="{!! route('parts.index') !!}" class="btn btn-default">Voltar</a>
    <button type="submit" class="btn btn-info pull-right">Salvar</button>
</div>
