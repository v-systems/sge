@extends('layouts.app')

@section('content-header', 'Peças')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Editar</h3>
			</div>
			{!! Form::model($part, ['route' => ['parts.update', $part->id], 'method' => 'PATCH', 'class' => 'form-horizontal', 'role' => 'form']) !!}
				@include('parts.form', [$part])
			{!! Form::close() !!}
		</div>
	</div>
</div>
@endsection
