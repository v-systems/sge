@extends('layouts.app')

@section('content-header', 'Peças')

@section('content')
	<div class="row">
		<div class="col-md-12">
			<div class="box">
				<div class="box-header with-border">
					<a type="button" class="btn btn-primary" href="{{ route('parts.create') }}"><i class="fa fa-plus"></i> Adicionar</a>
					<div class="clearfix">&nbsp;</div>
					<table class="table table-bordered table-striped" id="dataTableList">
						<thead>
							<tr>
                                @if (Auth::user()->role_id == 1)
                                    <td>Empresa</td>
                                @endif
								<td>Código</td>
								<td>Equipamento</td>
								<td>Descrição</td>
								<td>Data da Compra</td>
								<td>Computador instalado</td>
                                <td>Situação</td>
                                <td class="detail">N.º Série</td>
                                <td class="detail">IMEI</td>
                                <td class="detail">Número da NF</td>
                                <td class="detail">Fornecedor</td>
                                <td class="detail">Local Instalado</td>
                                <td class="detail">Departamento</td>
                                <td class="detail">Data da Instalação</td>
                                <td class="detail">Garantia</td>
								<td class="detail">Histórico</td>
								<td class="col-md-1">Ações</td>
							</tr>
						</thead>

						<tbody>
							@foreach ($parts as $part)
							    <tr>
                                    @if (Auth::user()->role_id == 1)
                                        <td>{{ $part->enterprise->name }}</td>
                                    @endif
							    	<td>{{ $part->code }}</td>
							    	<td>{{ $part->equipment->name }}</td>
							    	<td>{{ $part->description }}</td>
							    	<td>{{ $part->buy_date->format('d/m/Y') }}</td>
							    	<td>{{ $part->computer->name }}</td>
                                    <td>{{ $part->situation->name }}</td>
                                    <td class="detail">{{ $part->serial_number }}</td>
                                    <td class="detail">{{ $part->imei }}</td>
                                    <td class="detail">{{ $part->invoice_number }}</td>
                                    <td class="detail">{{ $part->supplier }}</td>
                                    <td class="detail">{{ $part->local->name }}</td>
                                    <td class="detail">{{ $part->department->name }}</td>
                                    <td class="detail">{{ $part->installation_date->format('d/m/Y') }}</td>
                                    <td class="detail">{{ $part->warranty }}</td>
                                    <td class="detail">{{ nl2br($part->historic) }}</td>
							    	<td nowrap="nowrap">
							    		{{ Form::open(['route' => ['parts.destroy', $part->id], 'method'=> 'delete', 'class' => 'form-inline']) }}
							    			@include('partials.actions', ['object' => $part, 'route' => 'parts'])
									    {{ Form::close() }}
							    	</td>
							    </tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
@endsection

