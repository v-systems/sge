@extends('layouts.app')

@section('content-header', 'Peças')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Visualizar</h3>
            </div>

            <div class="box-body form-horizontal">
                <div class="form-group">
                    {!! Form::label('code', 'Código', ['class' => 'control-label col-md-2']) !!}
                    <div class="col-md-10">
                        <p class="form-control-static">{!! $part->code !!}</p>
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('equipment_id', 'Equipamento', ['class' => 'control-label col-md-2']) !!}
                    {!! Form::label('equipment_id', 'Equipamento', ['class' => 'control-label col-md-2']) !!}
                    <div class="col-md-10">
                        <p class="form-control-static">{!! $part->equipment->name !!}</p>
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('description', 'Descrição', ['class' => 'control-label col-md-2']) !!}
                    <div class="col-md-10">
                        <p class="form-control-static">{!! $part->description !!}</p>
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('serial_number', 'N.º Série', ['class' => 'control-label col-md-2']) !!}
                    <div class="col-md-10">
                        <p class="form-control-static">{!! !empty($part->serial_number) ? $part->serial_number : '-' !!}</p>
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('imei', 'IMEI', ['class' => 'control-label col-md-2']) !!}
                    <div class="col-md-10">
                        <p class="form-control-static">{!! !empty($part->imei) ? $part->imei : '-' !!}</p>
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('buy_date', 'Data da Compra', ['class' => 'control-label col-md-2']) !!}
                    <div class="col-md-10">
                        <p class="form-control-static">{!! $part->buy_date->format('d/m/Y') !!}</p>
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('invoice_number', 'Número da NF', ['class' => 'control-label col-md-2']) !!}
                    <div class="col-md-10">
                        <p class="form-control-static">{!! $part->invoice_number !!}</p>
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('supplier', 'Fornecedor', ['class' => 'control-label col-md-2']) !!}
                    <div class="col-md-10">
                        <p class="form-control-static">{!! !empty($part->supplier) ? $part->supplier : '-' !!}</p>
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('local_id', 'Local Instalado', ['class' => 'control-label col-md-2']) !!}
                    <div class="col-md-10">
                        <p class="form-control-static">{!! $part->local->name !!}</p>
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('department_id', 'Departamento', ['class' => 'control-label col-md-2']) !!}
                    <div class="col-md-10">
                        <p class="form-control-static">{!! $part->department->name !!}</p>
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('computer_id', 'Comp./Local instalação', ['class' => 'control-label col-md-2']) !!}
                    <div class="col-md-10">
                        <p class="form-control-static">{!! $part->computer->name !!}</p>
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('situation_id', 'Situação', ['class' => 'control-label col-md-2']) !!}
                    <div class="col-md-10">
                        <p class="form-control-static">{!! $part->situation->name !!}</p>
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('installation_date', 'Data da Instalação', ['class' => 'control-label col-md-2']) !!}
                    <div class="col-md-10">
                        <p class="form-control-static">{!! $part->installation_date->format('d/m/Y') !!}</p>
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('warranty', 'Garantia', ['class' => 'control-label col-md-2']) !!}
                    <div class="col-md-10">
                        <p class="form-control-static">{!! $part->warranty !!}</p>
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('historic', 'Histórico', ['class' => 'control-label col-md-2']) !!}
                    <div class="col-md-10">
                        <p class="form-control-static">{!! !empty($part->historic) ? nl2br($part->historic) : '-' !!}</p>
                    </div>
                </div>
            </div>

            <div class="box-footer">
                <a href="{!! route('parts.index') !!}" class="btn btn-default">Voltar</a>
            </div>
        </div>
    </div>
</div>
@endsection

