{!! Form::hidden('id', null) !!}
<div class="box-body">

	<div class="form-group {!! $errors->has('name') ? 'has-error' : '' !!}">
		{!! Form::label('name', 'Nome', ['class' => 'control-label col-md-2']) !!}
		<div class="col-md-10">
			{!! Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => 'Nome']) !!}
            @if ($errors->has('name'))
                <span class="help-block">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
            @endif
		</div>
	</div>
    <div class="form-group {!! $errors->has('label') ? 'has-error' : '' !!}">
        {!! Form::label('label', 'Nome Público', ['class' => 'control-label col-md-2']) !!}
        <div class="col-md-10">
            {!! Form::text('label', old('label'), ['class' => 'form-control', 'placeholder' => 'Nome Público']) !!}
            @if ($errors->has('label'))
                <span class="help-block">
                    <strong>{{ $errors->first('label') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group {!! $errors->has('label') ? 'has-error' : '' !!}">
        {!! Form::label('permissions', 'Permissões', ['class' => 'control-label col-md-2']) !!}
        <div class="col-md-10">
            <select name="permissions[]" class="form-control multi-select" multiple="multiple">
                <?php $currentController = '' ?>
                @foreach ($permissions as $controller => $controllers)
                    @if ($controller != $currentController)
                        <optgroup label="{{ $controller }}">
                    @endif

                    @foreach ($controllers as $id => $permission)
                        <option value="{{ $id }}"
                            @if (isset($role) || old('permissions'))
                                <?php $rolePermissions = old('permissions') ? collect(Request::old('permissions')) : $role->permissions ?>
                                @if ($rolePermissions->contains($id))
                                    selected="selected"
                                @endif
                            @endif
                        >{{ $permission }}</option>
                    @endforeach

                    @if ($controller != $currentController)
                        <?php $currentController = $controller ?>
                        </optgroup>
                    @endif
                @endforeach
            </select>
            @if ($errors->has('permissions'))
                <span class="help-block">
                    <strong>{{ $errors->first('permissions') }}</strong>
                </span>
            @endif
        </div>
    </div>
</div>

<div class="box-footer">
    <a href="{!! route('roles.index') !!}" class="btn btn-default">Voltar</a>
    <button type="submit" class="btn btn-info pull-right">Salvar</button>
</div>
