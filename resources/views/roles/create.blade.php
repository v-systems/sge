@extends('layouts.app')

@section('content-header', 'Perfil')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Adicionar</h3>
			</div>
			{!! Form::open(['route' => ['roles.store'], 'class' => 'form-horizontal', 'role' => 'form']) !!}
				@include('roles.form')
			{!! Form::close() !!}
		</div>
	</div>
</div>
@endsection
