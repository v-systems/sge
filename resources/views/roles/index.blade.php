@extends('layouts.app')

@section('content-header', 'Perfil')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <a type="button" class="btn btn-primary" href="{{ route('roles.create') }}"><i class="fa fa-plus"></i> Adicionar</a>
                    <div class="clearfix">&nbsp;</div>
                    <table class="table table-bordered table-striped" id="dataTableList">
                        <thead>
                            <tr>
                                <td>Nome</td>
                                <td class="col-md-1">Ações</td>
                            </tr>
                        </thead>

                        <tbody>
                            @foreach ($roles as $role)
                                <tr>
                                    <td>{{ $role->label }}</td>
                                    <td nowrap="nowrap">
                                        {{ Form::open(['route' => ['roles.destroy', $role->id], 'method'=> 'delete', 'class' => 'form-inline']) }}
                                            {!! Form::hidden('id', $role->id) !!}
                                            <a type="button" class="btn btn-primary tooltips" data-toggle="tooltip" data-placement="top" title="Editar" href="{{ route('roles.edit', $role->id) }}"><i class="fa fa-edit"></i></a>
                                            <button type="button" class="btn btn-danger tooltips" data-toggle="modal" data-target="#modalDelete-{{ $role->id }}" title="Remover">
                                                <i class="fa fa-trash-o"></i>
                                            </button>
                                            <!-- Modal -->
                                            <div class="modal fade" id="modalDelete-{{ $role->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                                                 aria-hidden="true">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                            <h3 class="modal-title modal-title-delete">Atenção!</h3>
                                                        </div>
                                                        <div class="modal-body">
                                                            Deseja realmente remover?
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default btn-modal-close" data-dismiss="modal">Voltar</button>
                                                            <button type="submit" class="btn btn-danger">Remover</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        {{ Form::close() }}
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
