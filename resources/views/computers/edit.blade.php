@extends('layouts.app')

@section('content-header', 'Computador Instalado')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Editar</h3>
            </div>
            {!! Form::model($computer, ['route' => ['computers.update', $computer->id], 'method' => 'PATCH', 'class' => 'form-horizontal', 'role' => 'form']) !!}
                @include('computers.form')
            {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection
