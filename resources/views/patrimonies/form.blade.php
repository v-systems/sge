{!! Form::hidden('id', null) !!}
<div class="box-body">
    @include('partials.enterprise_field')

    <div class="form-group {!! $errors->has('code') ? 'has-error' : '' !!}">
        {!! Form::label('code', 'Código', ['class' => 'control-label col-md-2']) !!}
        <div class="col-md-10">
            {!! Form::text('code', old('code'), ['class' => 'form-control']) !!}
            @if ($errors->has('code'))
                <span class="help-block">
                    <strong>{{ $errors->first('code') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <div class="form-group {!! $errors->has('type_property_id') ? 'has-error' : '' !!}">
        {!! Form::label('type_property_id', 'Tipo do Bem', ['class' => 'control-label col-md-2']) !!}
        <div class="col-md-10">
            {!! Form::select('type_property_id', ['' => 'Selecione um Tipo do Bem']+$typeProperties, old('type_property_id'), ['class' => 'form-control']) !!}
            @if ($errors->has('type_property_id'))
                <span class="help-block">
                    <strong>{{ $errors->first('type_property_id') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <div class="form-group {!! $errors->has('description') ? 'has-error' : '' !!}">
        {!! Form::label('description', 'Descrição', ['class' => 'control-label col-md-2']) !!}
        <div class="col-md-10">
            {!! Form::text('description', old('description'), ['class' => 'form-control']) !!}
            @if ($errors->has('description'))
                <span class="help-block">
                    <strong>{{ $errors->first('description') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <div class="form-group {!! $errors->has('local_id') ? 'has-error' : '' !!}">
        {!! Form::label('local_id', 'Local', ['class' => 'control-label col-md-2']) !!}
        <div class="col-md-10">
            {!! Form::select('local_id', ['' => 'Selecione um Local']+$locals, old('local_id'), ['class' => 'form-control']) !!}
            @if ($errors->has('local_id'))
                <span class="help-block">
                    <strong>{{ $errors->first('local_id') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <div class="form-group {!! $errors->has('department_responsible_id') ? 'has-error' : '' !!}">
        {!! Form::label('department_responsible_id', 'Depart. Responsável', ['class' => 'control-label col-md-2']) !!}
        <div class="col-md-10">
            {!! Form::select('department_responsible_id', ['' => 'Selecione um Depart. Responsável']+$departmentResponsibles, old('department_responsible_id'), ['class' => 'form-control']) !!}
            @if ($errors->has('department_responsible_id'))
                <span class="help-block">
                    <strong>{{ $errors->first('department_responsible_id') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <div class="form-group {!! $errors->has('employee_id') ? 'has-error' : '' !!}">
        {!! Form::label('employee_id', 'Responsável', ['class' => 'control-label col-md-2']) !!}
        <div class="col-md-10">
            {!! Form::select('employee_id', ['' => 'Selecione um Funcionário']+$employees, old('employee_id'), ['class' => 'form-control']) !!}
            @if ($errors->has('employee_id'))
                <span class="help-block">
                    <strong>{{ $errors->first('employee_id') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <div class="form-group {!! $errors->has('department_use_id') ? 'has-error' : '' !!}">
        {!! Form::label('department_use_id', 'Depart. de Uso', ['class' => 'control-label col-md-2']) !!}
        <div class="col-md-10">
            {!! Form::select('department_use_id', ['' => 'Depart. de Uso']+$departmentUses, old('department_use_id'), ['class' => 'form-control']) !!}
            @if ($errors->has('department_use_id'))
                <span class="help-block">
                    <strong>{{ $errors->first('department_use_id') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <div class="form-group {!! $errors->has('nf') ? 'has-error' : '' !!}">
        {!! Form::label('nf', 'Nota Fiscal', ['class' => 'control-label col-md-2']) !!}
        <div class="col-md-10">
            {!! Form::text('nf', old('nf'), ['class' => 'form-control']) !!}
            @if ($errors->has('nf'))
                <span class="help-block">
                    <strong>{{ $errors->first('nf') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <div class="form-group {!! $errors->has('supplier') ? 'has-error' : '' !!}">
        {!! Form::label('supplier', 'Fornecedor', ['class' => 'control-label col-md-2']) !!}
        <div class="col-md-10">
            {!! Form::text('supplier', old('supplier'), ['class' => 'form-control']) !!}
            @if ($errors->has('supplier'))
                <span class="help-block">
                    <strong>{{ $errors->first('supplier') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <div class="form-group {!! $errors->has('value') ? 'has-error' : '' !!}">
        {!! Form::label('value', 'Valor', ['class' => 'control-label col-md-2']) !!}
        <div class="col-md-10">
            {!! Form::text('value', old('value'), ['class' => 'form-control money']) !!}
            @if ($errors->has('value'))
                <span class="help-block">
                    <strong>{{ $errors->first('value') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <div class="form-group {!! $errors->has('nf_date') ? 'has-error' : '' !!}">
        {!! Form::label('nf_date', 'Data da NF', ['class' => 'control-label col-md-2']) !!}
        <div class="col-md-10">
            {!! Form::text('nf_date', !empty($patrimony->nf_date) ? $patrimony->nf_date->format('d/m/Y') : old('nf_date'), ['class' => 'form-control datepicker']) !!}
            @if ($errors->has('nf_date'))
                <span class="help-block">
                    <strong>{{ $errors->first('nf_date') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <div class="form-group {!! $errors->has('release_date') ? 'has-error' : '' !!}">
        {!! Form::label('release_date', 'Data do Lançamento', ['class' => 'control-label col-md-2']) !!}
        <div class="col-md-10">
            {!! Form::text('release_date', !empty($patrimony->release_date) ? $patrimony->release_date->format('d/m/Y') : old('release_date'), ['class' => 'form-control datepicker']) !!}
            @if ($errors->has('release_date'))
                <span class="help-block">
                    <strong>{{ $errors->first('release_date') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <div class="form-group {!! $errors->has('situation_id') ? 'has-error' : '' !!}">
        {!! Form::label('situation_id', 'Situação', ['class' => 'control-label col-md-2']) !!}
        <div class="col-md-10">
            {!! Form::select('situation_id', ['' => 'Selecione uma Situação']+$situations, old('situation_id'), ['class' => 'form-control']) !!}
            @if ($errors->has('situation_id'))
                <span class="help-block">
                    <strong>{{ $errors->first('situation_id') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <div class="form-group {!! $errors->has('historic') ? 'has-error' : '' !!}">
        {!! Form::label('historic', 'Histórico', ['class' => 'control-label col-md-2']) !!}
        <div class="col-md-10">
            {!! Form::textarea('historic', !empty($patrimony->historic) ? $patrimony->historic : old('historic'), ['class' => 'form-control']) !!}
            @if ($errors->has('historic'))
                <span class="help-block">
                    <strong>{{ $errors->first('historic') }}</strong>
                </span>
            @endif
        </div>
    </div>

</div>

<div class="box-footer">
    <a href="{!! route('patrimonies.index') !!}" class="btn btn-default">Voltar</a>
    <button type="submit" class="btn btn-info pull-right">Salvar</button>
</div>
