@extends('layouts.app')

@section('content-header', 'Patrimônios')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <a type="button" class="btn btn-primary" href="{{ route('patrimonies.create') }}"><i class="fa fa-plus"></i> Adicionar</a>
                    <div class="clearfix">&nbsp;</div>
                    <table class="table table-bordered table-striped" id="dataTableList">
                        <thead>
                            <tr>
                                @if (Auth::user()->role_id == 1)
                                    <td>Empresa</td>
                                @endif
                                <td>Código</td>
                                <td>Tipo do Bem</td>
                                <td>Depart. Responsável</td>
                                <td>Depart. em Uso</td>
                                <td>Data Compra</td>
                                <td>NF</td>
                                <td>Data da NF</td>
                                <td>Fornecedor</td>
                                <td class="detail">Descrição</td>
                                <td class="detail">Local</td>
                                <td class="detail">Funcionário</td>
                                <td class="detail">Valor</td>
                                <td class="detail">Data do Lançamento</td>
                                <td class="detail">Situação</td>
                                <td class="detail">Histórico</td>
                                <td class="col-md-1">Ações</td>
                            </tr>
                        </thead>

                        <tbody>
                            @foreach ($patrimonies as $patrimony)
                                <tr>
                                    @if (Auth::user()->role_id == 1)
                                        <td>{{ $patrimony->enterprise->name }}</td>
                                    @endif
                                    <td>{{ $patrimony->code }}</td>
                                    <td>{{ $patrimony->typeProperty->name }}</td>
                                    <td>{{ $patrimony->departmentResponsible->name }}</td>
                                    <td>{{ $patrimony->departmentUse->name }}</td>
                                    <td>{{ $patrimony->release_date->format('d/m/Y') }}</td>
                                    <td>{{ $patrimony->nf }}</td>
                                    <td>{{ $patrimony->nf_date->format('d/m/Y') }}</td>
                                    <td>{{ $patrimony->supplier }}</td>
                                    <td class="detail">{{ $patrimony->description }}</td>
                                <td class="detail">{{ $patrimony->local->name }}</td>
                                <td class="detail">{{ $patrimony->employee->name }}</td>
                                <td class="detail">{{ $patrimony->value }}</td>
                                <td class="detail">{{ $patrimony->release_date->format('d/m/Y') }}</td>
                                <td class="detail">{{ $patrimony->situation->name }}</td>
                                <td class="detail">{{ $patrimony->historic }}</td>
                                    <td nowrap="nowrap">
                                        {{ Form::open(['route' => ['patrimonies.destroy', $patrimony->id], 'method'=> 'delete', 'class' => 'form-inline']) }}
                                            @include('partials.actions', ['object' => $patrimony, 'route' => 'patrimonies', 'historic' => true])
                                        {{ Form::close() }}
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
