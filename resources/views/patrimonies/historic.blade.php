@extends('layouts.app')

@section('content-header', $contentHeader)

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">
                    Adicionar Histórico
                    <a class="btn btn-xs btn-info" data-toggle="modal" href='#modal-id'>Ver detalhe do Patrimônio</a>
                </h3>
			</div>
			{!! Form::open(['route' => ['patrimonies.historic', $object->id], 'class' => 'form-horizontal', 'role' => 'form']) !!}
                <div class="box-body">
                    {!! Form::hidden('patrimony_id',$object->id) !!}
                    {!! Form::hidden('user_id', auth()->user()->id) !!}
                    <div class="form-group {!! $errors->has('historic') ? 'has-error' : '' !!}">
                        {!! Form::label('historic', 'Histórico', ['class' => 'control-label col-md-2']) !!}
                        <div class="col-md-10">
                            {!! Form::textarea('historic', '', ['class' => 'form-control']) !!}
                            @if ($errors->has('historic'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('historic') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="box-footer">
                    <a href="{!! route('patrimonies.index') !!}" class="btn btn-default">Voltar</a>
                    <button type="submit" class="btn btn-info pull-right">Salvar</button>
                </div>
			{!! Form::close() !!}
		</div>
	</div>
</div>

<div class="modal fade" id="modal-id">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Visualizar </h4>
            </div>
            <div class="modal-body">
                @include('patrimonies.show_fields', ['patrimony' => $object])
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

@endsection
