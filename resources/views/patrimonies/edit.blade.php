@extends('layouts.app')

@section('content-header', 'Patrimônios')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Editar</h3>
			</div>
			{!! Form::model($patrimony, ['route' => ['patrimonies.update', $patrimony->id], 'method' => 'PATCH', 'class' => 'form-horizontal', 'role' => 'form']) !!}
                @include('patrimonies.form', [$patrimony])
			{!! Form::close() !!}
		</div>
	</div>
</div>
@endsection
