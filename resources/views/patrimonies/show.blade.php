@extends('layouts.app')

@section('content-header', 'Patrimônios')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Visualizar</h3>
            </div>

            @include('patrimonies.show_fields', ['patrimony' => $patrimony])
        </div>

        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Histórico</h3>
            </div>

            <div class="box-body form-horizontal">
                <table class="table table-stripped">
                    <thead>
                        <th class="col-md-2">Data</th>
                        <th class="col-md-1">Usuário</th>
                        <th>Histórico</th>
                    </thead>

                    <tbody>
                        @foreach($patrimony->historics as $historic)
                            <tr>
                                <td align="center">{{ $historic->created_at->format('d/m/Y H:i') }}</td>
                                <td align="center" nowrap="nowrap">{{ $historic->user->name }}</td>
                                <td>{{ nl2br($historic->historic) }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

            <div class="box-footer">
                <a href="{!! route('patrimonies.index') !!}" class="btn btn-default">Voltar</a>
            </div>
        </div>
    </div>
</div>
@endsection
