<div class="box-body form-horizontal">
    @if (Auth::user()->role_id == 1)
        <div class="form-group">
            {!! Form::label('number', 'Empresa', ['class' => 'control-label col-md-2']) !!}
            <div class="col-md-10">
                <p class="form-control-static">{!! $patrimony->enterprise->name !!}</p>
            </div>
        </div>
    @endif

    <div class="form-group">
        {!! Form::label('number', 'Código', ['class' => 'control-label col-md-2']) !!}
        <div class="col-md-10">
            <p class="form-control-static">{!! $patrimony->code !!}</p>
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('chip_number', 'Tipo do Bem', ['class' => 'control-label col-md-2']) !!}
        <div class="col-md-10">
            <p class="form-control-static">{!! $patrimony->typeProperty->name !!}</p>
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('cellphone_plan_id', 'Descrição', ['class' => 'control-label col-md-2']) !!}
        <div class="col-md-10">
            <p class="form-control-static">{!! $patrimony->description !!}</p>
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('data', 'Local', ['class' => 'control-label col-md-2']) !!}
        <div class="col-md-10">
            <p class="form-control-static">{!! $patrimony->local->name !!}</p>
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('buy_date', 'Depart. Responsável', ['class' => 'control-label col-md-2']) !!}
        <div class="col-md-10">
            <p class="form-control-static">{!! $patrimony->departmentResponsible->name !!}</p>
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('invoice_number', 'Responsável', ['class' => 'control-label col-md-2']) !!}
        <div class="col-md-10">
            <p class="form-control-static">{!! $patrimony->employee->name !!}</p>
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('employee', 'Depart. de Uso', ['class' => 'control-label col-md-2']) !!}
        <div class="col-md-10">
            <p class="form-control-static">{!! $patrimony->departmentUse->name !!}</p>
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('job_id', 'NF', ['class' => 'control-label col-md-2']) !!}
        <div class="col-md-10">
            <p class="form-control-static">{!! $patrimony->nf !!}</p>
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('possession_date', 'Fornecedor', ['class' => 'control-label col-md-2']) !!}
        <div class="col-md-10">
            <p class="form-control-static">{!! $patrimony->supplier !!}</p>
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('line_situation_id', 'Valor', ['class' => 'control-label col-md-2']) !!}
        <div class="col-md-10">
            <p class="form-control-static">{!! $patrimony->value !!}</p>
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('line_situation_id', 'Data da NF', ['class' => 'control-label col-md-2']) !!}
        <div class="col-md-10">
            <p class="form-control-static">{!! $patrimony->nf_date->format('d/m/Y') !!}</p>
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('line_situation_id', 'Data do Lançamento', ['class' => 'control-label col-md-2']) !!}
        <div class="col-md-10">
            <p class="form-control-static">{!! $patrimony->release_date->format('d/m/Y') !!}</p>
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('line_situation_id', 'Situação', ['class' => 'control-label col-md-2']) !!}
        <div class="col-md-10">
            <p class="form-control-static">{!! $patrimony->situation->name !!}</p>
        </div>
    </div>
</div>
