@extends('layouts.app')

@section('content-header', 'Patrimônios')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Adicionar</h3>
			</div>
			{!! Form::open(['route' => ['patrimonies.store'], 'class' => 'form-horizontal', 'role' => 'form']) !!}
				@include('patrimonies.form')
			{!! Form::close() !!}
		</div>
	</div>
</div>
@endsection
