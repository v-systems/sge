@extends('layouts.app')

@section('content-header', 'Tipos de Bens')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <a type="button" class="btn btn-primary" href="{{ route('typeProperties.create') }}"><i class="fa fa-plus"></i> Adicionar</a>
                    <div class="clearfix">&nbsp;</div>
                    <table class="table table-bordered table-striped" id="dataTableList">
                        <thead>
                            <tr>
                                <td>Nome</td>
                                <td>Fixo</td>
                                <td class="col-md-1">Ações</td>
                            </tr>
                        </thead>

                        <tbody>
                            @foreach ($typeProperties as $typeProperty)
                                <tr>
                                    <td>{{ $typeProperty->name }}</td>
                                    <td>{{ $typeProperty->fixed ? 'Sim' : 'Não' }}</td>
                                    <td nowrap="nowrap">
                                        {{ Form::open(['route' => ['typeProperties.destroy', $typeProperty->id], 'method'=> 'delete', 'class' => 'form-inline']) }}
                                            @include('partials.actions', ['object' => $typeProperty, 'route' => 'typeProperties'])
                                        {{ Form::close() }}
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
