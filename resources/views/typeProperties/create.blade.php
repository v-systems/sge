@extends('layouts.app')

@section('content-header', 'Tipo de Bem')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Adicionar</h3>
			</div>
			{!! Form::open(['route' => ['typeProperties.store'], 'class' => 'form-horizontal', 'role' => 'form']) !!}
				@include('typeProperties.form')
			{!! Form::close() !!}
		</div>
	</div>
</div>
@endsection
