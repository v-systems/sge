<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="{{ ActiveMenu::isActiveURL('/') }}">
                <a href="{!! url('/') !!}">
                    <i class="ion ion-android-apps"></i> <span>Dashboard</span>
                </a>
            </li>
            <li class="{{ ActiveMenu::isActiveMatch('parts') }}"><a href="{{ route('parts.index') }}"><i class="ion ion-gear-b"></i>Peças</a></li>
            <li class="{{ ActiveMenu::isActiveMatch('chipNumbers') }}"><a href="{{ route('chipNumbers.index') }}"><i class="ion ion-android-call"></i>Números e Chips</a></li>
            <li class="{{ ActiveMenu::isActiveMatch('fixedips') }}"><a href="{{ route('fixedips.index') }}"><i class="ion ion-network"></i>IPs Fixos</a></li>
            <li class="{{ ActiveMenu::isActiveMatch('patrimonies') }}"><a href="{{ route('patrimonies.index') }}"><i class="fa fa-suitcase"></i>Patrimônios</a></li>

            <li class="treeview {{ ActiveMenu::areActiveMatchs(['equipments','departments','locals','situations','computers','typeProperties','employees']) }}">
                <a href="#">
                    <i class="ion ion-plus-circled"></i>
                    <span>Cadastros Gerais</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li class="{{ ActiveMenu::isActiveMatch('computers') }}"><a href="{{ route('computers.index') }}"><i class="fa fa-desktop"></i> Computador Instalado</a></li>
                    <li class="{{ ActiveMenu::isActiveMatch('equipments') }}"><a href="{{ route('equipments.index') }}"><i class="fa fa-hdd-o"></i> Equipamentos</a></li>
                    <li class="{{ ActiveMenu::isActiveMatch('departments') }}"><a href="{{ route('departments.index') }}"><i class="fa fa-tag"></i> Departamentos</a></li>
                    <li class="{{ ActiveMenu::isActiveMatch('locals') }}"><a href="{{ route('locals.index') }}"><i class="fa fa-map-marker"></i> Local Instalado</a></li>
                    <li class="{{ ActiveMenu::isActiveMatch('situations') }}"><a href="{{ route('situations.index') }}"><i class="fa fa-search"></i> Situação</a></li>
                    <li class="{{ ActiveMenu::isActiveMatch('employees') }}"><a href="{{ route('employees.index') }}"><i class="fa fa-user"></i> Funcionários</a></li>
                    <li class="{{ ActiveMenu::isActiveMatch('typeProperties') }}"><a href="{{ route('typeProperties.index') }}"><i class="fa fa-home"></i> Tipo de Bem</a></li>
                </ul>
            </li>

            <li class="treeview {{ ActiveMenu::areActiveMatchs(['roles', 'users', 'enterprises']) }}">
                <a href="#">
                    <i class="fa fa-laptop"></i>
                    <span>Controle de Acesso</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li class="{{ ActiveMenu::isActiveMatch('enterprises') }}"><a href="{{ route('enterprises.index') }}"><i class="fa fa-building-o"></i> Empresas</a></li>
                    <li class="{{ ActiveMenu::isActiveMatch('roles') }}"><a href="{{ route('roles.index') }}"><i class="ion ion-person-stalker"></i> Perfil</a></li>
                    <li class="{{ ActiveMenu::isActiveMatch('users') }}"><a href="{{ route('users.index') }}"><i class="ion ion-person"></i> Usuários</a></li>
                </ul>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
