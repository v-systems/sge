@if (Auth::user()->role_id == 1)
    <div class="form-group {!! $errors->has('enterprise_id') ? 'has-error' : '' !!}">
        {!! Form::label('enterprise_id', 'Empresa', ['class' => 'control-label col-md-2']) !!}
        <div class="col-md-10">
            {!! Form::select('enterprise_id', ['' => 'Selecione uma Empresa']+$enterprises, old('enterprise_id'), ['class' => 'form-control']) !!}
            @if ($errors->has('enterprise_id'))
                <span class="help-block">
                    <strong>{{ $errors->first('enterprise_id') }}</strong>
                </span>
            @endif
        </div>
    </div>
@else
    {!! Form::hidden('enterprise_id', Auth::user()->enterprise_id) !!}
@endif
