<header class="main-header">
    <!-- Logo -->
    <a href="{!! url('/') !!}" class="logo">SI<b>G</b></a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <span class="hidden-xs">{{ Auth::user()->name }} <i class="fa fa-angle-down pull-right"></i></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="{!! url('/logout') !!}"><i class="fa fa-sign-out"></i>Sair</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>
