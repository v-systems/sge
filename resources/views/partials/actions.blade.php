{!! Form::hidden('id', $object->id) !!}

@if (isset($historic) && $historic)
    <a type="button" class="btn btn-xs btn-info tooltips" data-tooltip="tooltip" data-placement="top" title="Adicionar Histórico" href="{{ route($route . '.historic', $object->id) }}"><i class="fa fa-comment-o"></i></a>
@endif

<a type="button" class="btn btn-xs btn-success tooltips" data-tooltip="tooltip" data-placement="top" title="Visualizar" href="{{ route($route . '.show', $object->id) }}"><i class="fa fa-file-text-o"></i></a>
<a type="button" class="btn btn-xs btn-primary tooltips" data-tooltip="tooltip" data-placement="top" title="Editar" href="{{ route($route . '.edit', $object->id) }}"><i class="fa fa-edit"></i></a>
<button type="button" class="btn btn-xs btn-danger tooltips" data-toggle="modal" data-tooltip="tooltip" data-target="#modalDelete-{{ $object->id }}" title="Remover">
    <i class="fa fa-trash-o"></i>
</button>
<!-- Modal -->
<div class="modal fade" id="modalDelete-{{ $object->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3 class="modal-title modal-title-delete">Atenção!</h3>
            </div>
            <div class="modal-body">
                Deseja realmente remover?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-modal-close" data-dismiss="modal">Voltar</button>
                <button type="submit" class="btn btn-danger">Remover</button>
            </div>
        </div>
    </div>
</div>
