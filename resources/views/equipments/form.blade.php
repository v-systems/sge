{!! Form::hidden('id', null) !!}
<div class="box-body">

	<div class="form-group {!! $errors->has('name') ? 'has-error' : '' !!}">
		{!! Form::label('name', 'Nome', ['class' => 'control-label col-md-2']) !!}
		<div class="col-md-10">
			{!! Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => 'Nome']) !!}
            @if ($errors->has('name'))
                <span class="help-block">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
            @endif
		</div>
	</div>
</div>

<div class="box-footer">
    <a href="{!! route('equipments.index') !!}" class="btn btn-default">Voltar</a>
    <button type="submit" class="btn btn-info pull-right">Salvar</button>
</div>
