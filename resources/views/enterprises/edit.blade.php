@extends('layouts.app')

@section('content-header', 'Empresas')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Editar</h3>
			</div>
			{!! Form::model($enterprise, ['route' => ['enterprises.update', $enterprise->id], 'method' => 'PATCH', 'class' => 'form-horizontal', 'role' => 'form']) !!}
				@include('enterprises.form')
			{!! Form::close() !!}
		</div>
	</div>
</div>
@endsection
