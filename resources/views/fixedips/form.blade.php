{!! Form::hidden('id', null) !!}
<div class="box-body">
    @include('partials.enterprise_field')

	<div class="form-group">
		{!! Form::label('ip_address', 'Ip', ['class' => 'control-label col-md-2']) !!}
		<div class="col-md-10">
			{!! Form::text('ip_address', old('ip_address'), ['class' => 'form-control ipaddress']) !!}
		</div>
	</div>

	<div class="form-group">
		{!! Form::label('hostname', 'Nome do Host', ['class' => 'control-label col-md-2']) !!}
		<div class="col-md-10">
			{!! Form::text('hostname', old('hostname'), ['class' => 'form-control']) !!}
		</div>
	</div>

	<!-- IP EQUIPMENT ID -->
	<div class="form-group {!! $errors->has('ip_equipment_id') ? 'has-error' : '' !!}">
		{!! Form::label('ip_equipment_id', 'Equipamento', ['class' => 'control-label col-md-2']) !!}
		<div class="col-md-10">
			{!! Form::select('ip_equipment_id', $ipsEquipments, old('ip_equipment_id'), ['class' => 'form-control']) !!}
			@if ($errors->has('ip_equipment_id'))
	            <span class="help-block">
	                <strong>{{ $errors->first('ip_equipment_id') }}</strong>
	            </span>
	        @endif
		</div>
	</div>

	<div class="form-group">
		{!! Form::label('equipment_type_id', 'Tipo Equipamento', ['class' => 'control-label col-md-2']) !!}
		<div class="col-md-10">
            {!! Form::select('equipment_type_id', $equipmentTypes, old('equipment_type_id'), ['class' => 'form-control']) !!}
		</div>
	</div>

	<div class="form-group">
		{!! Form::label('equipment_description', 'Descrição Equipamento', ['class' => 'control-label col-md-2']) !!}
		<div class="col-md-10">
			{!! Form::text('equipment_description', old('equipment_description'), ['class' => 'form-control']) !!}
		</div>
	</div>

	<!-- HOST MV ID -->
	<div class="form-group {!! $errors->has('host_mvs_id') ? 'has-error' : '' !!}">
		{!! Form::label('host_mvs_id', 'Hospedeiro MV', ['class' => 'control-label col-md-2']) !!}
		<div class="col-md-10">
			{!! Form::select('host_mvs_id', $hostMvs, old('host_mvs_id'), ['class' => 'form-control']) !!}
			@if ($errors->has('host_mvs_id'))
	            <span class="help-block">
	                <strong>{{ $errors->first('host_mvs_id') }}</strong>
	            </span>
	        @endif
		</div>
	</div>

	<div class="form-group">
		{!! Form::label('service_description', 'Descrição Serviços', ['class' => 'control-label col-md-2']) !!}
		<div class="col-md-10">
			{!! Form::text('service_description', old('service_description'), ['class' => 'form-control']) !!}
		</div>
	</div>

	<!-- LOCAL ID -->
	<div class="form-group {!! $errors->has('local_id') ? 'has-error' : '' !!}">
		{!! Form::label('local_id', 'Local', ['class' => 'control-label col-md-2']) !!}
		<div class="col-md-10">
			{!! Form::select('local_id', $locals, old('local_id'), ['class' => 'form-control']) !!}
			@if ($errors->has('local_id'))
	            <span class="help-block">
	                <strong>{{ $errors->first('local_id') }}</strong>
	            </span>
	        @endif
		</div>
	</div>

	<!-- DEPARTMENT ID -->
	<div class="form-group {!! $errors->has('department_id') ? 'has-error' : '' !!}">
		{!! Form::label('department_id', 'Departamento', ['class' => 'control-label col-md-2']) !!}
		<div class="col-md-10">
			{!! Form::select('department_id', $departments, old('department_id'), ['class' => 'form-control']) !!}
			@if ($errors->has('department_id'))
	            <span class="help-block">
	                <strong>{{ $errors->first('department_id') }}</strong>
	            </span>
	        @endif
		</div>
	</div>
</div>

<div class="box-footer">
    <a href="{!! route('fixedips.index') !!}" class="btn btn-default">Voltar</a>
    <button type="submit" class="btn btn-info pull-right">Salvar</button>
</div>



