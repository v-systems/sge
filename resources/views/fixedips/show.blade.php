@extends('layouts.app')

@section('content-header', 'Ips Fixos')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Visualizar</h3>
            </div>

            <div class="box-body form-horizontal">
                <div class="form-group">
                    {!! Form::label('ip_address', 'Ip', ['class' => 'control-label col-md-2']) !!}
                    <div class="col-md-10">
                        <p class="form-control-static">{!! $fixedip->ip_address !!}</p>
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('hostname', 'Nome do Host', ['class' => 'control-label col-md-2']) !!}
                    <div class="col-md-10">
                        <p class="form-control-static">{!! $fixedip->hostname !!}</p>
                    </div>
                </div>

                <!-- IP EQUIPAMENT ID -->
                <div class="form-group">
                    {!! Form::label('ip_equipment_id', 'Equipamento', ['class' => 'control-label col-md-2']) !!}
                    <div class="col-md-10">
                        <p class="form-control-static">{!! $fixedip->ipEquipment->name !!}</p>
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('equipment_type_id', 'Tipo Equipamento', ['class' => 'control-label col-md-2']) !!}
                    <div class="col-md-10">
                        <p class="form-control-static">{!! $fixedip->equipmentType->name !!}</p>
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('equipment_description', 'Descrição Equipamento', ['class' => 'control-label col-md-2']) !!}
                    <div class="col-md-10">
                        <p class="form-control-static">{!! $fixedip->equipment_description !!}</p>
                    </div>
                </div>

                <!-- HOST MV ID -->
                <div class="form-group">
                    {!! Form::label('host_mvs_id', 'Hospedeiro MV', ['class' => 'control-label col-md-2']) !!}
                    <div class="col-md-10">
                        <p class="form-control-static">{!! $fixedip->hostMv->name !!}</p>
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('service_description', 'Descrição Serviços', ['class' => 'control-label col-md-2']) !!}
                    <div class="col-md-10">
                        <p class="form-control-static">{!! $fixedip->service_description !!}</p>
                    </div>
                </div>

                <!-- LOCAL ID -->
                <div class="form-group">
                    {!! Form::label('local_id', 'Local', ['class' => 'control-label col-md-2']) !!}
                    <div class="col-md-10">
                        <p class="form-control-static">{!! $fixedip->local->name !!}</p>
                    </div>
                </div>

                <!-- DEPARTMENT ID -->
                <div class="form-group">
                    {!! Form::label('department_id', 'Departamento', ['class' => 'control-label col-md-2']) !!}
                    <div class="col-md-10">
                        <p class="form-control-static">{!! $fixedip->department->name !!}</p>
                    </div>
                </div>
            </div>

            <div class="box-footer">
                <a href="{!! route('fixedips.index') !!}" class="btn btn-default">Voltar</a>
            </div>
        </div>
    </div>
</div>
@endsection
