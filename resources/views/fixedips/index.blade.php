@extends('layouts.app')

@section('content-header', 'Ips Fixos')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <a type="button" class="btn btn-primary" href="{{ route('fixedips.create') }}"><i class="fa fa-plus"></i> Adicionar</a>
                    <div class="clearfix">&nbsp;</div>
                    <table class="table table-bordered table-striped" id="dataTableList">
                        <thead>
                            <tr>
                                @if (Auth::user()->role_id == 1)
                                    <td>Empresa</td>
                                @endif
                                <td>Ip</td>
                                <td>Nome do Host</td>
                                <td>Equipamento</td>
                                <td>Tipo de Equipamento</td>
                                <td>Hospedeiro MV</td>
                                <td>Local</td>
                                <td>Departamento</td>
                                <td class="col-md-1">Ações</td>
                            </tr>
                        </thead>

                        <tbody>
                            @foreach ($fixedips as $fixedip)
                                <tr>
                                    @if (Auth::user()->role_id == 1)
                                        <td>{{ $fixedip->enterprise->name }}</td>
                                    @endif
                                    <td>{{ $fixedip->ip_address }}</td>
                                    <td>{{ $fixedip->hostname }}</td>
                                    <td>{{ $fixedip->ipEquipment->name }}</td>
                                    <td>{{ $fixedip->equipmentType->name }}</td>
                                    <td>{{ $fixedip->hostMv->name }}</td>
                                    <td>{{ $fixedip->local->name }}</td>
                                    <td>{{ $fixedip->department->name }}</td>
                                    <td nowrap="nowrap">
                                        {{ Form::open(['route' => ['fixedips.destroy', $fixedip->id], 'method'=> 'delete', 'class' => 'form-inline']) }}
                                            @include('partials.actions', ['object' => $fixedip, 'route' => 'fixedips'])
                                        {{ Form::close() }}
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
