@extends('layouts.app')

@section('content-header', 'Ips Fixos')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Editar</h3>
			</div>
			{!! Form::model($fixedip, ['route' => ['fixedips.update', $fixedip->id], 'method' => 'PATCH', 'class' => 'form-horizontal', 'role' => 'form']) !!}
				@include('fixedips.form', [$fixedip])
			{!! Form::close() !!}
		</div>
	</div>
</div>
@endsection
