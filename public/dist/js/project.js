Project = {
	init: function () {
		$(".datepicker").datepicker({
			format: 'dd/mm/yyyy',
			language: 'pt-BR'
		});

		var dataTableList = $("#dataTableList").DataTable({
			"language": {
				"url": "/plugins/datatables/Portuguese-Brasil.json"
			},
			"paging": true,
			"lengthChange": true,
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "Tudo"]],
			"searching": true,
			"ordering": true,
			"info": true,
			"autoWidth": false,
            dom: 'Bflrtip',
            buttons: [
                {
                    extend: 'excel',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'print',
                    text: 'Imprimir',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'colvis',
                    text: 'Colunas Visíveis'
                }
            ],
		});

        dataTableList.columns( '.detail' ).visible( false );

        $('.multi-select').multiSelect({selectableOptgroup: true});

        $('.ipaddress').mask('0ZZ.0ZZ.0ZZ.0ZZ', {
            translation: {
                'Z': {
                    pattern: /[0-9]/, optional: true
                }
            }
        });

        $('.money').mask('000.000.000.000.000,00', {reverse: true});

        $('body').tooltip({
            selector: "[data-tooltip=tooltip]",
            container: "body"
        });
	}
}

Project.init();
