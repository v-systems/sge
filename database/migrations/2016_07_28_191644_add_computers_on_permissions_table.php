<?php

use App\Permission;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddComputersOnPermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Equipaments
        Permission::create(['name' => 'ComputersController@index', 'label' => 'Listar', 'controller' => 'Computador Instalado']);
        Permission::create(['name' => 'ComputersController@create', 'label' => 'Tela de Adição', 'controller' => 'Computador Instalado']);
        Permission::create(['name' => 'ComputersController@store', 'label' => 'Adicionar', 'controller' => 'Computador Instalado']);
        Permission::create(['name' => 'ComputersController@show', 'label' => 'Visualizar', 'controller' => 'Computador Instalado']);
        Permission::create(['name' => 'ComputersController@edit', 'label' => 'Tela de Edição', 'controller' => 'Computador Instalado']);
        Permission::create(['name' => 'ComputersController@update', 'label' => 'Editar', 'controller' => 'Computador Instalado']);
        Permission::create(['name' => 'ComputersController@destroy', 'label' => 'Remover', 'controller' => 'Computador Instalado']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Permission::where('name', 'like', 'ComputersController%')->delete();
    }
}
