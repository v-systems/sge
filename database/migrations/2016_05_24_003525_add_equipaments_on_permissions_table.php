<?php

use App\Permission;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEquipamentsOnPermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Equipaments
        Permission::create(['name' => 'EquipamentsController@index', 'label' => 'Listar', 'controller' => 'Equipamentos']);
        Permission::create(['name' => 'EquipamentsController@create', 'label' => 'Tela de Adição', 'controller' => 'Equipamentos']);
        Permission::create(['name' => 'EquipamentsController@store', 'label' => 'Adicionar', 'controller' => 'Equipamentos']);
        Permission::create(['name' => 'EquipamentsController@show', 'label' => 'Visualizar', 'controller' => 'Equipamentos']);
        Permission::create(['name' => 'EquipamentsController@edit', 'label' => 'Tela de Edição', 'controller' => 'Equipamentos']);
        Permission::create(['name' => 'EquipamentsController@update', 'label' => 'Editar', 'controller' => 'Equipamentos']);
        Permission::create(['name' => 'EquipamentsController@destroy', 'label' => 'Remover', 'controller' => 'Equipamentos']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Permission::where('name', 'like', 'EquipamentsController%')->delete();
    }
}
