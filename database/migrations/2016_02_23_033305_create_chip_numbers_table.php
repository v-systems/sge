<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChipNumbersTable extends Migration
{
    public function up()
    {
        Schema::create('chip_numbers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cellphone_plan_id')->unsigned();
            $table->integer('job_id')->unsigned();
            $table->integer('line_situation_id')->unsigned();
            $table->integer('enterprise_id')->unsigned();
            $table->string('number')->nullable();
            $table->string('chip_number')->nullable();
            $table->string('data')->nullable();
            $table->date('buy_date')->nullable();
            $table->string('invoice_number')->nullable();
            $table->string('employee')->nullable();
            $table->date('possession_date')->nullable();
            $table->longText('historic')->nullable();

            // foreign keys
            $table->foreign('cellphone_plan_id')->references('id')->on('cellphone_plans')->onDelete('cascade');
            $table->foreign('line_situation_id')->references('id')->on('line_situations')->onDelete('cascade');
            $table->foreign('job_id')->references('id')->on('jobs')->onDelete('cascade');
            $table->foreign('enterprise_id')->references('id')->on('enterprises')->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('chip_numbers');
    }
}
