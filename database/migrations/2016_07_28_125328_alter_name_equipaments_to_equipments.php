<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterNameEquipamentsToEquipments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        // Drop Foreign
        Schema::table('fixed_ips', function (Blueprint $table) {
            $table->dropForeign('fixed_ips_ip_equipament_id_foreign');
            $table->dropForeign('fixed_ips_equipament_type_id_foreign');
            $table->renameColumn('ip_equipament_id', 'ip_equipment_id');
            $table->renameColumn('equipament_type_id', 'equipment_type_id');
            $table->renameColumn('equipament_description', 'equipment_description');
        });

        Schema::table('parts', function (Blueprint $table) {
            $table->dropForeign('parts_equipament_id_foreign');
            $table->renameColumn('equipament_id', 'equipment_id');
        });

        // Rename Tables
        if (Schema::hasTable('equipaments')) {
            Schema::rename('equipaments', 'equipment');
        }

        if (Schema::hasTable('equipament_types')) {
            Schema::rename('equipament_types', 'equipment_types');
        }

        if (Schema::hasTable('ip_equipaments')) {
            Schema::rename('ip_equipaments', 'ip_equipments');
        }

        // New Foreigns
        Schema::table('fixed_ips', function (Blueprint $table) {
            $table->foreign('equipment_type_id')->references('id')->on('equipment_types')->onDelete('cascade');
            $table->foreign('ip_equipment_id')->references('id')->on('ip_equipments')->onDelete('cascade');
        });

        Schema::table('parts', function (Blueprint $table) {
            $table->foreign('equipment_id')->references('id')->on('equipment')->onDelete('cascade');
        });

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        // Drop Foreign
        Schema::table('fixed_ips', function (Blueprint $table) {
            $table->dropForeign('fixed_ips_ip_equipment_id_foreign');
            $table->dropForeign('fixed_ips_equipment_type_id_foreign');
            $table->renameColumn('ip_equipment_id', 'ip_equipament_id');
            $table->renameColumn('equipment_type_id', 'equipament_type_id');
            $table->renameColumn('equipment_description', 'equipament_description');
        });

        Schema::table('parts', function (Blueprint $table) {
            $table->dropForeign('parts_equipment_id_foreign');
            $table->renameColumn('equipment_id', 'equipament_id');
        });

        // Rename Tables
        if (Schema::hasTable('equipment')) {
            Schema::rename('equipment', 'equipaments');
        }

        if (Schema::hasTable('equipment_types')) {
            Schema::rename('equipment_types', 'equipament_types');
        }

        if (Schema::hasTable('ip_equipments')) {
            Schema::rename('ip_equipments', 'ip_equipaments');
        }

        // New Foreigns
        Schema::table('fixed_ips', function (Blueprint $table) {
            $table->foreign('equipament_type_id')->references('id')->on('equipament_types')->onDelete('cascade');
            $table->foreign('ip_equipament_id')->references('id')->on('ip_equipaments')->onDelete('cascade');
        });

        Schema::table('parts', function (Blueprint $table) {
            $table->foreign('equipament_id')->references('id')->on('equipaments')->onDelete('cascade');
        });

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
