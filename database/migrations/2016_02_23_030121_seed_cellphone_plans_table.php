<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SeedCellphonePlansTable extends Migration
{
    public function up()
    {
        DB::table('cellphone_plans')->insert([
            ['name' => 'Liberty 0', 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'Liberty 50', 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'Liberty 100', 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'Liberty 200', 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'Liberty 400', 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'Mundi 100', 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'Mundi 400', 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'Dados Vivo', 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'Pré-pago claro', 'description' => '', 'created_at' => date('Y-m-d H:i:s')]
        ]);
    }

    public function down()
    {
        return true;
    }
}
