<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SeedIpEquipamentsTable extends Migration
{
    public function up()
    {
        DB::table('ip_equipaments')->insert([
            ['name' => 'APPLIANCE', 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'COMPUTADOR', 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'DVR', 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'IMPRESSORA', 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'MODEM ADSL', 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'RELOGIO DE PONTO', 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'ROTEADOR/RADIO', 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'SERVIDOR', 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'SWITCH', 'description' => '', 'created_at' => date('Y-m-d H:i:s')]
        ]);
    }

    public function down()
    {
        return true;
    }
}
