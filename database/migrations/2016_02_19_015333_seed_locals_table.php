<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SeedLocalsTable extends Migration
{
    public function up()
    {
        DB::table('locals')->insert([
            ['name' => 'Distribuidora', 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'Fabrica', 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'Loja', 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
        ]);
    }

    public function down()
    {
        return true;
    }
}

