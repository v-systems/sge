<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTypePropertiesTable extends Migration
{
    public function up()
    {
        Schema::create('type_properties', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->boolean('fixed');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('type_properties');
    }
}
