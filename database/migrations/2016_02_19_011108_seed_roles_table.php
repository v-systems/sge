<?php

use App\Role;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class SeedRolesTable extends Migration
{
    public function up()
    {
        $role = new Role;
        $role->name = 'admin';
        $role->label = 'Administrador';
        $role->save();
    }

    public function down()
    {
        return true;
    }
}
