<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFixedIpsTable extends Migration
{
    public function up()
    {
        Schema::create('fixed_ips', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('equipament_type_id')->unsigned();
            $table->integer('ip_equipament_id')->unsigned();
            $table->integer('host_mvs_id')->unsigned();
            $table->integer('local_id')->unsigned();
            $table->integer('department_id')->unsigned();
            $table->integer('enterprise_id')->unsigned();
            $table->string('ip_address')->nullable();
            $table->string('hostname')->nullable();
            $table->longText('equipament_description')->nullable();
            $table->longText('service_description')->nullable();

            // foreign keys
            $table->foreign('equipament_type_id')->references('id')->on('equipament_types')->onDelete('cascade');
            $table->foreign('ip_equipament_id')->references('id')->on('ip_equipaments')->onDelete('cascade');
            $table->foreign('host_mvs_id')->references('id')->on('host_mvs')->onDelete('cascade');
            $table->foreign('local_id')->references('id')->on('locals')->onDelete('cascade');
            $table->foreign('department_id')->references('id')->on('departments')->onDelete('cascade');
            $table->foreign('enterprise_id')->references('id')->on('enterprises')->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('fixed_ips');
    }
}
