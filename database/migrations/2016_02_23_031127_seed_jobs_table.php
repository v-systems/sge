<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SeedJobsTable extends Migration
{
    public function up()
    {
        DB::table('jobs')->insert([
            ['name' => 'Vendedor', 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'Motorista', 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'Funcionário', 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'Supervisor', 'description' => '', 'created_at' => date('Y-m-d H:i:s')]
        ]);
    }

    public function down()
    {
        return true;
    }
}
