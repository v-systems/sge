<?php

use App\Permission;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSituationsOnPermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Equipaments
        Permission::create(['name' => 'SituationsController@index', 'label' => 'Listar', 'controller' => 'Situação']);
        Permission::create(['name' => 'SituationsController@create', 'label' => 'Tela de Adição', 'controller' => 'Situação']);
        Permission::create(['name' => 'SituationsController@store', 'label' => 'Adicionar', 'controller' => 'Situação']);
        Permission::create(['name' => 'SituationsController@show', 'label' => 'Visualizar', 'controller' => 'Situação']);
        Permission::create(['name' => 'SituationsController@edit', 'label' => 'Tela de Edição', 'controller' => 'Situação']);
        Permission::create(['name' => 'SituationsController@update', 'label' => 'Editar', 'controller' => 'Situação']);
        Permission::create(['name' => 'SituationsController@destroy', 'label' => 'Remover', 'controller' => 'Situação']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Permission::where('name', 'like', 'SituationsController%')->delete();
    }
}
