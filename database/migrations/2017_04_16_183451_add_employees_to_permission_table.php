<?php

use App\Permission;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEmployeesToPermissionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Employees
        Permission::create(['name' => 'EmployeesController@index', 'label' => 'Listar', 'controller' => 'Funcionários']);
        Permission::create(['name' => 'EmployeesController@create', 'label' => 'Tela de Adição', 'controller' => 'Funcionários']);
        Permission::create(['name' => 'EmployeesController@store', 'label' => 'Adicionar', 'controller' => 'Funcionários']);
        Permission::create(['name' => 'EmployeesController@show', 'label' => 'Visualizar', 'controller' => 'Funcionários']);
        Permission::create(['name' => 'EmployeesController@edit', 'label' => 'Tela de Edição', 'controller' => 'Funcionários']);
        Permission::create(['name' => 'EmployeesController@update', 'label' => 'Editar', 'controller' => 'Funcionários']);
        Permission::create(['name' => 'EmployeesController@destroy', 'label' => 'Remover', 'controller' => 'Funcionários']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Permission::where('name', 'like', 'EmployeesController%')->delete();
    }
}
