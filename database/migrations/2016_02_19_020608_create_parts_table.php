<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePartsTable extends Migration
{
    public function up()
    {
        Schema::create('parts', function (Blueprint $table)
        {
            $table->increments('id');
            $table->integer('equipament_id')->unsigned();
            $table->integer('department_id')->unsigned();
            $table->integer('computer_id')->unsigned();
            $table->integer('situation_id')->unsigned();
            $table->integer('local_id')->unsigned();
            $table->integer('enterprise_id')->unsigned();
            $table->longText('description')->nullable();
            $table->string('serial_number')->nullable();
            $table->string('code')->unique();
            $table->string('imei')->nullable();
            $table->date('buy_date')->nullable();
            $table->string('invoice_number')->nullable();
            $table->string('supplier')->nullable();
            $table->date('installation_date')->nullable();
            $table->string('warranty',50)->nullable();
            $table->longText('historic')->nullable();

            // foreign keys
            $table->foreign('equipament_id')->references('id')->on('equipaments')->onDelete('cascade');
            $table->foreign('local_id')->references('id')->on('locals')->onDelete('cascade');
            $table->foreign('department_id')->references('id')->on('departments')->onDelete('cascade');
            $table->foreign('computer_id')->references('id')->on('computers')->onDelete('cascade');
            $table->foreign('situation_id')->references('id')->on('situations')->onDelete('cascade');
            $table->foreign('enterprise_id')->references('id')->on('enterprises')->onDelete('cascade');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('parts');
    }
}
