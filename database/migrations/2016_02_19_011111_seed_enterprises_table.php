<?php

use App\Enterprise;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SeedEnterprisesTable extends Migration
{
    public function up()
    {
        $enterprise = new Enterprise;
        $enterprise->name = 'RuralShop';
        $enterprise->description = '';
        $enterprise->save();
    }

    public function down()
    {
        return true;
    }
}
