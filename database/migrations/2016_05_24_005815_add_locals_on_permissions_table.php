<?php

use App\Permission;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLocalsOnPermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Equipaments
        Permission::create(['name' => 'LocalsController@index', 'label' => 'Listar', 'controller' => 'Local Instalado']);
        Permission::create(['name' => 'LocalsController@create', 'label' => 'Tela de Adição', 'controller' => 'Local Instalado']);
        Permission::create(['name' => 'LocalsController@store', 'label' => 'Adicionar', 'controller' => 'Local Instalado']);
        Permission::create(['name' => 'LocalsController@show', 'label' => 'Visualizar', 'controller' => 'Local Instalado']);
        Permission::create(['name' => 'LocalsController@edit', 'label' => 'Tela de Edição', 'controller' => 'Local Instalado']);
        Permission::create(['name' => 'LocalsController@update', 'label' => 'Editar', 'controller' => 'Local Instalado']);
        Permission::create(['name' => 'LocalsController@destroy', 'label' => 'Remover', 'controller' => 'Local Instalado']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Permission::where('name', 'like', 'LocalsController%')->delete();
    }
}
