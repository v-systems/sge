<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SeedSituationsTable extends Migration
{
    public function up()
    {
        DB::table('situations')->insert([
            ['name' => 'EM OPERAÇÃO' , 'description' => '','created_at' => date('Y-m-d H:i:s')],
            ['name' => 'EM MANUTENÇÃO' , 'description' => '','created_at' => date('Y-m-d H:i:s')],
            ['name' => 'EM STANDBY' , 'description' => '','created_at' => date('Y-m-d H:i:s')],
            ['name' => 'VENDIDO' , 'description' => '','created_at' => date('Y-m-d H:i:s')],
            ['name' => 'DESCARTADO' , 'description' => '','created_at' => date('Y-m-d H:i:s')],
        ]);
    }

    public function down()
    {
        return true;
    }
}
