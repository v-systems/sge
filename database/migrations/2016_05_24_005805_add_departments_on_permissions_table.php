<?php

use App\Permission;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDepartmentsOnPermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Departments
        Permission::create(['name' => 'DepartmentsController@index', 'label' => 'Listar', 'controller' => 'Departamentos']);
        Permission::create(['name' => 'DepartmentsController@create', 'label' => 'Tela de Adição', 'controller' => 'Departamentos']);
        Permission::create(['name' => 'DepartmentsController@store', 'label' => 'Adicionar', 'controller' => 'Departamentos']);
        Permission::create(['name' => 'DepartmentsController@show', 'label' => 'Visualizar', 'controller' => 'Departamentos']);
        Permission::create(['name' => 'DepartmentsController@edit', 'label' => 'Tela de Edição', 'controller' => 'Departamentos']);
        Permission::create(['name' => 'DepartmentsController@update', 'label' => 'Editar', 'controller' => 'Departamentos']);
        Permission::create(['name' => 'DepartmentsController@destroy', 'label' => 'Remover', 'controller' => 'Departamentos']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Permission::where('name', 'like', 'DepartmentsController%')->delete();
    }
}
