<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SeedLineSituationTable extends Migration
{
    public function up()
    {
         DB::table('line_situations')->insert([
            ['name' => 'Ativo', 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'Desativado', 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'Bloqueio temporário', 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'Linha cancelada', 'description' => '', 'created_at' => date('Y-m-d H:i:s')]
        ]);
    }

    public function down()
    {
        return true;
    }
}
