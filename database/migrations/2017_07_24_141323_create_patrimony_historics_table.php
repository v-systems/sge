<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePatrimonyHistoricsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patrimony_historics', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('patrimony_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->text('historic');
            $table->timestamps();

            // Foreigns
            $table->foreign('patrimony_id')->references('id')->on('patrimonies')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('patrimony_historics');
    }
}
