<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SeedDepartmentsTable extends Migration
{
    public function up()
    {
        DB::table('departments')->insert([
            ['name' => 'Análise vendas', 'description' => '', 'created_at'=> date("Y-m-d H:i:s")],
            ['name' => 'Apoio a vendas', 'description' => '', 'created_at'=> date("Y-m-d H:i:s")],
            ['name' => 'Balcão', 'description' => '', 'created_at'=> date("Y-m-d H:i:s")],
            ['name' => 'Caixa', 'description' => '', 'created_at'=> date("Y-m-d H:i:s")],
            ['name' => 'Compras', 'description' => '', 'created_at'=> date("Y-m-d H:i:s")],
            ['name' => 'Contabilidade', 'description' => '', 'created_at'=> date("Y-m-d H:i:s")],
            ['name' => 'Contas a pagar', 'description' => '', 'created_at'=> date("Y-m-d H:i:s")],
            ['name' => 'Contas a receber', 'description' => '', 'created_at'=> date("Y-m-d H:i:s")],
            ['name' => 'Data Center', 'description' => '', 'created_at'=> date("Y-m-d H:i:s")],
            ['name' => 'Diretoria', 'description' => '', 'created_at'=> date("Y-m-d H:i:s")],
            ['name' => 'Estoque', 'description' => '', 'created_at'=> date("Y-m-d H:i:s")],
            ['name' => 'Fábrica Comral', 'description' => '', 'created_at'=> date("Y-m-d H:i:s")],
            ['name' => 'Fábrica Comral', 'description' => '', 'created_at'=> date("Y-m-d H:i:s")],
            ['name' => 'Faturamento', 'description' => '', 'created_at'=> date("Y-m-d H:i:s")],
            ['name' => 'Gerência', 'description' => '', 'created_at'=> date("Y-m-d H:i:s")],
            ['name' => 'Logística', 'description' => '', 'created_at'=> date("Y-m-d H:i:s")],
            ['name' => 'Recepção', 'description' => '', 'created_at'=> date("Y-m-d H:i:s")],
            ['name' => 'Revenda', 'description' => '', 'created_at'=> date("Y-m-d H:i:s")],
            ['name' => 'Suporte T.I.', 'description' => '', 'created_at'=> date("Y-m-d H:i:s")],
            ['name' => 'Vendas externas', 'description' => '', 'created_at'=> date("Y-m-d H:i:s")],
            ['name' => '<Não aplicável>', 'description' => '', 'created_at'=> date("Y-m-d H:i:s")],
        ]);
    }

    public function down()
    {
        return true;
    }
}
