<?php

use App\Permission;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPatrimoniesOnPermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Patrimonies
        Permission::create(['name' => 'PatrimoniesController@index', 'label' => 'Listar', 'controller' => 'Patrimônio']);
        Permission::create(['name' => 'PatrimoniesController@create', 'label' => 'Tela de Adição', 'controller' => 'Patrimônio']);
        Permission::create(['name' => 'PatrimoniesController@store', 'label' => 'Adicionar', 'controller' => 'Patrimônio']);
        Permission::create(['name' => 'PatrimoniesController@show', 'label' => 'Visualizar', 'controller' => 'Patrimônio']);
        Permission::create(['name' => 'PatrimoniesController@edit', 'label' => 'Tela de Edição', 'controller' => 'Patrimônio']);
        Permission::create(['name' => 'PatrimoniesController@update', 'label' => 'Editar', 'controller' => 'Patrimônio']);
        Permission::create(['name' => 'PatrimoniesController@destroy', 'label' => 'Remover', 'controller' => 'Patrimônio']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Permission::where('name', 'like', 'PatrimoniesController%')->delete();
    }
}
