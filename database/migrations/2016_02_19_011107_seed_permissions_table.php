<?php

use App\Permission;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class SeedPermissionsTable extends Migration
{
    public function up()
    {
        $label_index = 'Listar';
        $label_create = 'Tela de Adição';
        $label_store = 'Adicionar';
        $label_show = 'Visualizar';
        $label_edit = 'Tela de Edição';
        $label_update = 'Editar';
        $label_destroy = 'Remover';

        // Users
        Permission::create(['name' => 'UsersController@index', 'label' => $label_index, 'controller' => 'Usuários']);
        Permission::create(['name' => 'UsersController@create', 'label' => $label_create, 'controller' => 'Usuários']);
        Permission::create(['name' => 'UsersController@store', 'label' => $label_store, 'controller' => 'Usuários']);
        Permission::create(['name' => 'UsersController@edit', 'label' => $label_edit, 'controller' => 'Usuários']);
        Permission::create(['name' => 'UsersController@update', 'label' => $label_update, 'controller' => 'Usuários']);
        Permission::create(['name' => 'UsersController@destroy', 'label' => $label_destroy, 'controller' => 'Usuários']);

        // Roles
        Permission::create(['name' => 'RolesController@index', 'label' => $label_index, 'controller' => 'Perfil']);
        Permission::create(['name' => 'RolesController@create', 'label' => $label_create, 'controller' => 'Perfil']);
        Permission::create(['name' => 'RolesController@store', 'label' => $label_store, 'controller' => 'Perfil']);
        Permission::create(['name' => 'RolesController@edit', 'label' => $label_edit, 'controller' => 'Perfil']);
        Permission::create(['name' => 'RolesController@update', 'label' => $label_update, 'controller' => 'Perfil']);
        Permission::create(['name' => 'RolesController@destroy', 'label' => $label_destroy, 'controller' => 'Perfil']);

        // Enterprises
        Permission::create(['name' => 'EnterprisesController@index', 'label' => $label_index, 'controller' => 'Empresas']);
        Permission::create(['name' => 'EnterprisesController@create', 'label' => $label_create, 'controller' => 'Empresas']);
        Permission::create(['name' => 'EnterprisesController@store', 'label' => $label_store, 'controller' => 'Empresas']);
        Permission::create(['name' => 'EnterprisesController@edit', 'label' => $label_edit, 'controller' => 'Empresas']);
        Permission::create(['name' => 'EnterprisesController@update', 'label' => $label_update, 'controller' => 'Empresas']);
        Permission::create(['name' => 'EnterprisesController@destroy', 'label' => $label_destroy, 'controller' => 'Empresas']);

        // Parts
        Permission::create(['name' => 'PartsController@index', 'label' => $label_index, 'controller' => 'Peças']);
        Permission::create(['name' => 'PartsController@create', 'label' => $label_create, 'controller' => 'Peças']);
        Permission::create(['name' => 'PartsController@store', 'label' => $label_store, 'controller' => 'Peças']);
        Permission::create(['name' => 'PartsController@show', 'label' => $label_show, 'controller' => 'Peças']);
        Permission::create(['name' => 'PartsController@edit', 'label' => $label_edit, 'controller' => 'Peças']);
        Permission::create(['name' => 'PartsController@update', 'label' => $label_update, 'controller' => 'Peças']);
        Permission::create(['name' => 'PartsController@destroy', 'label' => $label_destroy, 'controller' => 'Peças']);

        // Chip And Numbers
        Permission::create(['name' => 'ChipNumbersController@index', 'label' => $label_index, 'controller' => 'Números e Chips']);
        Permission::create(['name' => 'ChipNumbersController@create', 'label' => $label_create, 'controller' => 'Números e Chips']);
        Permission::create(['name' => 'ChipNumbersController@store', 'label' => $label_store, 'controller' => 'Números e Chips']);
        Permission::create(['name' => 'ChipNumbersController@show', 'label' => $label_show, 'controller' => 'Números e Chips']);
        Permission::create(['name' => 'ChipNumbersController@edit', 'label' => $label_edit, 'controller' => 'Números e Chips']);
        Permission::create(['name' => 'ChipNumbersController@update', 'label' => $label_update, 'controller' => 'Números e Chips']);
        Permission::create(['name' => 'ChipNumbersController@destroy', 'label' => $label_destroy, 'controller' => 'Números e Chips']);

        // Fixed IPS
        Permission::create(['name' => 'FixedIpsController@index', 'label' => $label_index, 'controller' => 'IPs Fixos']);
        Permission::create(['name' => 'FixedIpsController@create', 'label' => $label_create, 'controller' => 'IPs Fixos']);
        Permission::create(['name' => 'FixedIpsController@store', 'label' => $label_store, 'controller' => 'IPs Fixos']);
        Permission::create(['name' => 'FixedIpsController@show', 'label' => $label_show, 'controller' => 'IPs Fixos']);
        Permission::create(['name' => 'FixedIpsController@edit', 'label' => $label_edit, 'controller' => 'IPs Fixos']);
        Permission::create(['name' => 'FixedIpsController@update', 'label' => $label_update, 'controller' => 'IPs Fixos']);
        Permission::create(['name' => 'FixedIpsController@destroy', 'label' => $label_destroy, 'controller' => 'IPs Fixos']);
    }

    public function down()
    {
        return true;
    }
}
