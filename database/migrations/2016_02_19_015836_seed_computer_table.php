<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SeedComputerTable extends Migration
{
    public function up()
    {
        DB::table('computers')->insert([
            ['name' => 'ANALISEVENDAS01' , 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'ANALISEVENDAS02' , 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'APOIOVENDAS01' , 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'APOIOVENDAS02' , 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'APOIOVENDAS03' , 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'APOIOVENDAS04' , 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'BALCAO01' , 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'BALCAO02' , 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'BALCAO03' , 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'BALCAO04' , 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'BALCAO05' , 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'CAIXA01' , 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'CAIXA02' , 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'CHECKOUT01' , 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'CHECKOUT02' , 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'CHECKOUTDI01' , 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'CHECKOUTDI02' , 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'CHECKOUTDI03' , 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'COLETOR01' , 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'COLETOR02' , 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'COLETOR03' , 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'COLETOR04' , 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'COLETOR05' , 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'COLETOR06' , 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'COLETOR07' , 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'COLETOR08' , 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'COMPRAS01' , 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'COMPRAS02' , 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'COMPRAS03' , 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'COMPRAS04' , 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'CONTABILIDADE01' , 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'CONTABILIDADE02' , 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'CONTABILIDADE03' , 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'CONTABILIDADE04' , 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'CONTABILIDADE05' , 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'CONTABILIDADE06' , 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'CONTASREC01' , 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'CONTASREC02' , 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'CONTASREC03' , 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'DIRETORIA01' , 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'DIRETORIA02' , 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'ESTOQUE01' , 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'EXPEDICAO01' , 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'EXPEDICAO02' , 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'EXPEDICAO03' , 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'EXPEDICAODI01' , 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'EXPEDICAODI02' , 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'EXPEDICAODI03' , 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'FATURA01' , 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'FATURA02' , 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'GERENCIA01' , 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'GERENCIA02' , 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'IMPRESSORA' , 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'LOGISTICA01' , 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'LOGISTICA02' , 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'PAULO' , 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'RACK' , 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'REVENDA01' , 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'REVENDA02' , 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'REVENDA03' , 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'REVENDA04' , 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'RH01' , 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'RH02' , 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'SERVDIST01' , 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'SERVDIST02' , 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'SERVDIST03' , 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'SERVDIST04' , 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'SERVDIST05' , 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'SERVDIST06' , 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'SUPORTETI01' , 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'SUPORTETI02' , 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'SUPORTETI03' , 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => '<NÃO APLICÁVEL>', 'description' => '', 'created_at' => date('Y-m-d H:i:s')]

        ]);
    }

    public function down()
    {
        return true;
    }
}
