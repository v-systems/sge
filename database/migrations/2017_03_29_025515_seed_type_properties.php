<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SeedTypeProperties extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('type_properties')->insert([
            ['name' => 'Ferramenta', 'fixed' => 0, 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'Imobiliário', 'fixed' => 1, 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'Veículo', 'fixed' => 0, 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'Equipamento de Informática', 'fixed' => 0, 'created_at' => date('Y-m-d H:i:s')]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        return true;
    }
}
