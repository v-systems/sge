<?php

use App\Permission;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterEquipmentsPermission extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $permissions = Permission::where('controller', 'Equipamentos')->get();

        foreach ($permissions as $permission) {
            $name = explode('@', $permission->name);
            $new_name = "EquipmentsController@" . $name[1];;
            $permission->name = $new_name;
            $permission->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $permissions = Permission::where('controller', 'Equipamentos')->get();

        foreach ($permissions as $permission) {
            $name = explode('@', $permission->name);
            $new_name = "EquipamentsController@" . $name[1];;
            $permission->name = $new_name;
            $permission->save();
        }
    }
}
