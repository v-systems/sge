<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterEquipmentToEquipmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Rename Tables
        if (Schema::hasTable('equipment')) {
            Schema::rename('equipment', 'equipments');
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Rename Tables
        if (Schema::hasTable('equipments')) {
            Schema::rename('equipments', 'equipment');
        }
    }
}
