<?php

use App\Permission;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPatrimonyHistoricOnPermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Patrimonies
        Permission::create(['name' => 'PatrimoniesController@historic', 'label' => 'Adicionar Histórico', 'controller' => 'Patrimônio']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Permission::where('name', 'like', 'PatrimoniesController@historic')->delete();
    }
}
