<?php

use App\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class SeedUsersTable extends Migration
{
    public function up()
    {
        $user = new User;
        $user->name = 'Administrador';
        $user->role_id = '1';
        $user->enterprise_id = '1';
        $user->email = 'admin@admin.com';
        $user->password = 'admin';
        $user->save();
    }

    public function down()
    {
        return true;
    }
}
