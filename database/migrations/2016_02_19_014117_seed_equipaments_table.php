<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SeedEquipamentsTable extends Migration
{
    public function up()
    {
        DB::table('equipaments')->insert([
            ['name' => 'Antivírus', 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'Bateria coletor', 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'Bateria no-break', 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'Calculadora', 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'Celular', 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'Coletor', 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'Computador', 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'Disco rígido', 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'Drive DVD', 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'DVR', 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'Estabilizador', 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'FAX', 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'Fonte', 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'Gabinete', 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'Gaveta Caixa', 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'Impressora', 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'Leitor cheque', 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'Leitor cod barras', 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'Memória', 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'Modem ADSL', 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'Monitor', 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'Mouse', 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'No-break', 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'Notebook', 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'PinPad', 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'Placa de rede', 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'Placa de vídeo', 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'Placa-mãe', 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'Processador', 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'Projetor', 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'Roteador sem fio', 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'Servidor', 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'Smartphone', 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'Switch', 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'Tablet', 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'Teclado', 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'Telefone', 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'Tonner', 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'Transformador', 'description' => '', 'created_at' => date('Y-m-d H:i:s')]
        ]);
    }

    public function down()
    {
        return true;
    }
}
