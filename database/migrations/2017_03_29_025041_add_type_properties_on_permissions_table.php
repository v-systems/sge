<?php

use App\Permission;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTypePropertiesOnPermissionsTable extends Migration
{
    public function up()
    {
        // TypeProperties
        Permission::create(['name' => 'TypePropertiesController@index', 'label' => 'Listar', 'controller' => 'Tipo de Bem']);
        Permission::create(['name' => 'TypePropertiesController@create', 'label' => 'Tela de Adição', 'controller' => 'Tipo de Bem']);
        Permission::create(['name' => 'TypePropertiesController@store', 'label' => 'Adicionar', 'controller' => 'Tipo de Bem']);
        Permission::create(['name' => 'TypePropertiesController@show', 'label' => 'Visualizar', 'controller' => 'Tipo de Bem']);
        Permission::create(['name' => 'TypePropertiesController@edit', 'label' => 'Tela de Edição', 'controller' => 'Tipo de Bem']);
        Permission::create(['name' => 'TypePropertiesController@update', 'label' => 'Editar', 'controller' => 'Tipo de Bem']);
        Permission::create(['name' => 'TypePropertiesController@destroy', 'label' => 'Remover', 'controller' => 'Tipo de Bem']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Permission::where('name', 'like', 'TypePropertiesController%')->delete();
    }
}
