<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePatrimoniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patrimonies', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('enterprise_id')->unsigned();
            $table->integer('department_responsible_id')->unsigned();
            $table->integer('department_use_id')->unsigned();
            $table->integer('situation_id')->unsigned();
            $table->integer('local_id')->unsigned();
            $table->integer('employee_id')->unsigned();
            $table->integer('type_property_id')->unsigned();
            $table->string('description');
            $table->string('nf');
            $table->string('supplier');
            $table->decimal('value', 15, 2);
            $table->string('nf_date');
            $table->string('release_date');
            $table->longText('historic')->nullable();

            // foreign keys
            $table->foreign('enterprise_id')->references('id')->on('enterprises')->onDelete('cascade');
            $table->foreign('department_responsible_id')->references('id')->on('departments')->onDelete('cascade');
            $table->foreign('department_use_id')->references('id')->on('departments')->onDelete('cascade');
            $table->foreign('situation_id')->references('id')->on('situations')->onDelete('cascade');
            $table->foreign('local_id')->references('id')->on('locals')->onDelete('cascade');
            $table->foreign('employee_id')->references('id')->on('employees')->onDelete('cascade');
            $table->foreign('type_property_id')->references('id')->on('type_properties')->onDelete('cascade');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patrimonies');
    }
}
