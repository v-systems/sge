<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SeedEquipamentTypesTable extends Migration
{
    public function up()
    {
        DB::table('equipament_types')->insert([
            ['name' => 'Equipamento Físico', 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'Máquina Virtual', 'description' => '', 'created_at' => date('Y-m-d H:i:s')]
        ]);
    }

    public function down()
    {
        return true;
    }
}
