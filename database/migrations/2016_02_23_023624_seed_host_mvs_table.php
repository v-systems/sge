<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SeedHostMvsTable extends Migration
{
    public function up()
    {
        DB::table('host_mvs')->insert([
            ['name' => 'SERVDIST01', 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'SERVDIST02', 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'SERVDIST03', 'description' => '', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'SERVDIST04', 'description' => '', 'created_at' => date('Y-m-d H:i:s')]
        ]);
    }

    public function down()
    {
        return true;
    }
}
