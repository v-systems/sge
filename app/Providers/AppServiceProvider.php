<?php

namespace App\Providers;

use App\Patrimony;
use App\PatrimonyHistoric;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Patrimony::created(function ($patrimony) {
            $user = auth()->user();
            $historic = "Cadastro realizado.";
            PatrimonyHistoric::create(['patrimony_id' => $patrimony->id, 'user_id' => $user->id, 'historic' => $historic]);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
