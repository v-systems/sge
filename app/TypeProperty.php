<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeProperty extends Model
{
    protected $fillable = [
        'name',
        'fixed'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function patrimonies()
    {
        return $this->belongsTo('App\Patrimony');
    }
}
