<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FixedIp extends Model
{

    protected $table = 'fixed_ips';

    protected $fillable = [
    	'ip_address',
    	'hostname',
    	'ip_equipment_id',
    	'equipment_type_id',
    	'equipment_description',
    	'host_mvs_id',
    	'service_description',
    	'local_id',
    	'department_id',
        'enterprise_id'
    ];

    protected $dates = [
    	'created_at',
    	'updated_at',
    	'deleted_at'
    ];

    public function enterprise()
    {
        return $this->belongsTo('App\Enterprise');
    }

    public function equipmentType()
    {
        return $this->belongsTo('App\EquipmentType');
    }

    public function ipEquipment()
    {
        return $this->belongsTo('App\IpEquipment');
    }

    public function hostMv()
    {
        return $this->belongsTo('App\HostMv', 'host_mvs_id');
    }

    public function department()
    {
        return $this->belongsTo('App\Department');
    }

    public function local()
    {
        return $this->belongsTo('App\Local');
    }
}
