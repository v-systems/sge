<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Hash;

class User extends Authenticatable
{
    use SoftDeletes;

    protected $fillable = [
        'role_id', 'name', 'username', 'email', 'password', 'enterprise_id'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }

    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    public function enterprise()
    {
        return $this->belongsTo(Enterprise::class);
    }

    public function assignRole($role)
    {
        $this->role_id = $role->id;
        return $this->save();
    }

    public function hasRole($roles)
    {
        if (is_string($roles)) {
            return $this->role->name == $roles;
        }

        return !! $roles->intersect($this->role()->get())->count();
    }
}
