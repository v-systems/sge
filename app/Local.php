<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Local extends Model
{
    protected $fillable = [
    	'name',
    	'description'
    ];

    protected $dates = [
    	'created_at',
    	'updated_at',
    	'deleted_at'
    ];

    public function parts()
    {
    	return $this->belongsTo('App\Part');
    }
}
