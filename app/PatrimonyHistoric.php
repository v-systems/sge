<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PatrimonyHistoric extends Model
{
    protected $fillable = [
        'patrimony_id',
        'user_id',
        'historic',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    public function patrimony()
    {
        return $this->belongsTo('App\Patrimony');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
