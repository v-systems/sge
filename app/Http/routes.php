
<?php

Route::group(['middleware' => 'web'], function () {
    Route::auth();

    Route::group(['middleware' => 'auth'], function () {
        Route::get('/', 'HomeController@index');
        Route::get('/dashboard', 'HomeController@index');

        Route::group(['middleware' => 'authorize'], function () {
            /* ACLs */
            Route::resource('enterprises', 'EnterprisesController');
            Route::resource('users', 'UsersController');
            Route::resource('roles', 'RolesController');

            Route::resource('equipments', 'EquipmentsController');
            Route::resource('departments', 'DepartmentsController');
            Route::resource('locals', 'LocalsController');
            Route::resource('situations', 'SituationsController');
            Route::resource('computers', 'ComputersController');
            Route::resource('parts', 'PartsController');
            Route::resource('fixedips', 'FixedIpsController');
            Route::resource('chipNumbers', 'ChipNumbersController');
            Route::resource('typeProperties', 'TypePropertiesController');
            Route::resource('patrimonies', 'PatrimoniesController');
            Route::get('patrimonies/{id}/historic', 'PatrimoniesController@historic')->name('patrimonies.historic');
            Route::post('patrimonies/{id}/historic', 'PatrimoniesController@saveHistoric')->name('patrimonies.historic');
            Route::resource('employees', 'EmployeesController');
        });
    });
});
