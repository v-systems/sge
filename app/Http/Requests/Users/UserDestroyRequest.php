<?php

namespace App\Http\Requests\Users;

use Auth;
use App\Http\Requests\Request;

class UserDestroyRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $user = Auth::user();

        return ['id' => 'not_in:' . $user->id];
    }
}
