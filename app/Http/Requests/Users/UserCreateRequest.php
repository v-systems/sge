<?php

namespace App\Http\Requests\Users;

use App\Http\Requests\Request;

class UserCreateRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'confirmed|required'
        ];
    }
}
