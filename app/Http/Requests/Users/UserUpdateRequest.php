<?php

namespace App\Http\Requests\Users;

use App\User;
use App\Http\Requests\Request;

class UserUpdateRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $user = User::findOrFail($this->request->get('id'));

        return [
            'name' => 'required',
            'email' => 'required|email|unique:users,email,' . $this->request->get('id'),
            'password' => 'confirmed'
        ];
    }

    public function all()
    {
        $input = parent::all();

        if (empty($input['password']) && empty($input['password_confirmation'])) {
            unset($input['password']);
            unset($input['password_confirmation']);
        }

        return $input;
    }
}
