<?php

namespace App\Http\Requests\Enterprises;

use App\Http\Requests\Request;

class EnterprisesCreateRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required|unique:enterprises,name,NULL,id,deleted_at,NULL',
            'description' => ''
        ];
    }
}
