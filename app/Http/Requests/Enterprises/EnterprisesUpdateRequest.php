<?php

namespace App\Http\Requests\Enterprises;

use App\Http\Requests\Request;

class EnterprisesUpdateRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required|unique:enterprises,name,NULL,' . $this->request->get('id') . ',deleted_at,NULL',
            'description' => ''
        ];
    }
}
