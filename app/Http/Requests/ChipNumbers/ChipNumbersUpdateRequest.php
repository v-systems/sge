<?php

namespace App\Http\Requests\ChipNumbers;

use App\Http\Requests\Request;

class ChipNumbersUpdateRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'enterprise_id' => 'required',
            'number' => 'required|max:10|unique:chip_numbers,number,' . $this->request->get('id') . ',id,enterprise_id,' . $this->request->get('enterprise_id') . ',deleted_at,NULL',
            'chip_number' => 'required|max:255',
            'cellphone_plan_id' => 'required',
            'data' => 'required',
            'buy_date' => 'required|date_format:d/m/Y',
            'invoice_number' => 'max:255',
            'job_id' => 'required',
            'employee' => 'required|max:255',
            'line_situation_id' => 'required',
            'possession_date' => 'required|date_format:d/m/Y',
            'historic' => '',
        ];
    }
}
