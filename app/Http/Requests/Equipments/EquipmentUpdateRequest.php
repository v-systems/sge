<?php

namespace App\Http\Requests\Equipments;

use App\Http\Requests\Request;

class EquipmentUpdateRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required|unique:equipments,name,NULL,' . $this->request->get('id') . ',deleted_at,NULL',
            'description' => ''
        ];
    }
}
