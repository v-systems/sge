<?php

namespace App\Http\Requests\Equipments;

use App\Http\Requests\Request;

class EquipmentCreateRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required|unique:equipments,name,NULL,id,deleted_at,NULL'
        ];
    }
}
