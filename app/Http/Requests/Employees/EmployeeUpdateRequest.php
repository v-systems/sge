<?php

namespace App\Http\Requests\Employees;

use App\Http\Requests\Request;

class EmployeeUpdateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'enterprise_id' => 'required',
            'department_id' => 'required',
            'name' => 'required|unique:employees,name,' . $this->request->get('id') . ',id,enterprise_id,' . $this->request->get('enterprise_id') . ',deparment_id,' . $this->request->get('enterprise_id') . ',deleted_at,NULL',
        ];
    }
}
