<?php

namespace App\Http\Requests\Locals;

use App\Http\Requests\Request;

class LocalCreateRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required|unique:locals,name,NULL,id,deleted_at,NULL',
            'description' => ''
        ];
    }
}
