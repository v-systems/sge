<?php

namespace App\Http\Requests\Computers;

use App\Http\Requests\Request;

class ComputerUpdateRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required|unique:computers,name,NULL,' . $this->request->get('id') . ',deleted_at,NULL',
            'description' => ''
        ];
    }
}
