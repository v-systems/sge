<?php

namespace App\Http\Requests\Computers;

use App\Http\Requests\Request;

class ComputerCreateRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required|unique:computers,name,NULL,id,deleted_at,NULL',
            'description' => ''
        ];
    }
}
