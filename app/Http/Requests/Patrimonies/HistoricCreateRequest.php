<?php

namespace App\Http\Requests\Patrimonies;

use App\Http\Requests\Request;

class HistoricCreateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'patrimony_id' => 'required',
            'user_id' => 'required',
            'historic' => 'required'
        ];
    }
}
