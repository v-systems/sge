<?php

namespace App\Http\Requests\Patrimonies;

use App\Http\Requests\Request;

class PatrimonyUpdateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'enterprise_id' => 'required',
            'type_property_id' => 'required',
            'situation_id' => 'required',
            'local_id' => 'required',
            'department_responsible_id' => 'required',
            'employee_id' => 'required',
            'department_use_id' => 'required',
            'code' => 'required',
            'description' => 'required',
            'nf' => 'required',
            'supplier' => 'required',
            'value' => 'required',
            'nf_date' => 'required|date_format:d/m/Y',
            'release_date' => 'required|date_format:d/m/Y',
        ];
    }
}
