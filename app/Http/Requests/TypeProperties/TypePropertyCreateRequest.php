<?php

namespace App\Http\Requests\TypeProperties;

use App\Http\Requests\Request;

class TypePropertyCreateRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required|unique:type_properties,name,NULL,id,deleted_at,NULL',
        ];
    }
}
