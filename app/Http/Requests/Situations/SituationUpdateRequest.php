<?php

namespace App\Http\Requests\Situations;

use App\Http\Requests\Request;

class SituationUpdateRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required|unique:situations,name,NULL,' . $this->request->get('id') . ',deleted_at,NULL',
            'description' => ''
        ];
    }
}
