<?php

namespace App\Http\Requests\FixedIps;

use App\Http\Requests\Request;

class FixedIpsCreateRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'enterprise_id' => 'required',
            'ip_address' => 'required|unique:fixed_ips,ip_address,NULL,id,enterprise_id,' . $this->request->get('enterprise_id') . ',deleted_at,NULL',
            'hostname' => 'required|max:255',
            'ip_equipment_id' => 'required|numeric',
            'equipment_type_id' => 'required',
            'equipment_description' => 'required',
            'host_mvs_id' => 'required|numeric',
            'service_description' => 'required',
            'local_id' => 'required|numeric',
            'department_id' => 'required|numeric',
        ];
    }
}
