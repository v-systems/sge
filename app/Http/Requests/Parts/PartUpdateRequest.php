<?php

namespace App\Http\Requests\Parts;

use App\Http\Requests\Request;

class PartUpdateRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'enterprise_id' => 'required',
            'code' => 'required|max:12|unique:parts,code,' . $this->request->get('id') . ',id,enterprise_id,' . $this->request->get('enterprise_id') . ',deleted_at,NULL',
            'equipment_id' => 'required',
            'description' => 'required',
            'serial_number',
            'imei',
            'buy_date' => 'required',
            'invoice_number' => 'required',
            'supplier',
            'local_id' => 'required',
            'department_id' => 'required',
            'computer_id' => 'required',
            'situation_id' => 'required',
            'installation_date' => 'required',
            'warranty' => 'required',
            // 'historic' => 'required'
        ];
    }
}
