<?php

namespace App\Http\Requests\Departments;

use App\Http\Requests\Request;

class DepartmentCreateRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required|unique:departments,name,NULL,id,deleted_at,NULL',
            'description' => ''
        ];
    }
}
