<?php

namespace App\Http\Requests\Departments;

use App\Http\Requests\Request;

class DepartmentUpdateRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required|unique:departments,name,NULL,' . $this->request->get('id') . ',deleted_at,NULL',
            'description' => ''
        ];
    }
}
