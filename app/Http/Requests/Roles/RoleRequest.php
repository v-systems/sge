<?php

namespace App\Http\Requests\Roles;

use App\Http\Requests\Request;

class RoleRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $id = $this->request->get('id') ? $this->request->get('id') : 'NULL';
        return [
            'name' => 'required',
            'label' => 'required',
            'permissions' => 'required',
        ];
    }
}
