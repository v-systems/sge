<?php

namespace App\Http\Controllers;

use App\TypeProperty;

use App\Http\Requests\TypeProperties\TypePropertyCreateRequest;
use App\Http\Requests\TypeProperties\TypePropertyUpdateRequest;

use App\Http\Requests;

class TypePropertiesController extends Controller
{
    private $model;

    public function __construct(TypeProperty $typeProperty)
    {
        $this->model = $typeProperty;
    }

    public function index()
    {
        return view('typeProperties.index', ['typeProperties' => $this->model->get()]);
    }

    public function create()
    {
        return view('typeProperties.create');
    }

    public function store(TypePropertyCreateRequest $request)
    {
        $model = $this->model->create($request->all());

        if ($model->id) {
            return redirect()->route('typeProperties.index')->with('success', 'Tipo de Bem cadastrado com sucesso.');
        }

        return back();
    }

    public function edit($id)
    {
        $typeProperty = $this->model->findOrFail($id);
        return view('typeProperties.edit', compact('typeProperty'));
    }

    public function show($id)
    {
        $typeProperty = $this->model->findOrFail($id);

        return view('typeProperties.show', compact('typeProperty'));
    }

    public function update(TypePropertyUpdateRequest $request, $id)
    {
        if ($this->model->findOrFail($id)->update($request->all())) {
            return redirect()->route('typeProperties.index')->with('success', 'Tipo de Bem atualizado com sucesso.');
        }

        return back()->withInput()->with('error', 'Ocorreu um erro ao tentar atualizar o registro.');
    }

    public function destroy($id)
    {
        if ($this->model->findOrFail($id)->delete()) {
            return redirect()->route('typeProperties.index')->with('success', 'Tipo de Bem removido com sucesso.');
        }

        return back()->with('error', 'Tipo de Bem não foi removido.');
    }
}
