<?php

namespace App\Http\Controllers;

use App\Department;
use App\Employee;
use App\Http\Requests\Patrimonies\PatrimonyCreateRequest;
use App\Http\Requests\Patrimonies\PatrimonyUpdateRequest;
use App\Http\Requests\Patrimonies\HistoricCreateRequest;
use App\Local;
use App\Patrimony;
use App\Situation;
use App\TypeProperty;
use App\PatrimonyHistoric;
use Illuminate\Http\Request;

class PatrimoniesController extends Controller
{

    private $model;

    public function __construct(Patrimony $model)
    {
        parent::__construct();

        $this->model = $model;
    }

    private function mountSelects()
    {
        $typeProperties = TypeProperty::orderBy('name')->lists('name', 'id')->toArray();
        $locals = Local::orderBy('name')->lists('name', 'id')->toArray();
        $employees = Employee::orderBy('name')->lists('name', 'id')->toArray();
        $departmentResponsibles = Department::orderBy('name')->lists('name', 'id')->toArray();
        $departmentUses = Department::orderBy('name')->lists('name', 'id')->toArray();
        $situations = Situation::orderBy('name')->lists('name', 'id')->toArray();
        \View::share(compact('typeProperties', 'locals', 'employees', 'departmentResponsibles', 'departmentUses', 'situations'));
    }

    public function index()
    {
        return view('patrimonies.index', ['patrimonies' => $this->model->get()]);
    }

    public function create()
    {
        $this->mountSelects();
        return view('patrimonies.create');
    }

    public function store(PatrimonyCreateRequest $request)
    {
        $model = $this->model->create($request->all());

        $this->model->addHistoric($request, $model);

        if ($model->id) {
            return redirect()->route('patrimonies.index')->with('success', 'Patrimônio cadastrado com sucesso.');
        }

        return back();
    }

    public function edit($id)
    {
        $this->mountSelects();
        $patrimony = $this->model->findOrFail($id);
        return view('patrimonies.edit', compact('patrimony'));
    }

    public function show($id)
    {
        $patrimony = $this->model->findOrFail($id);

        return view('patrimonies.show', compact('patrimony'));
    }

    public function update(PatrimonyUpdateRequest $request, $id)
    {
        $model = $this->model->findOrFail($id);
        if ($model->update($request->all())) {
            $model->addHistoric($request, $model);
            return redirect()->route('patrimonies.index')->with('success', 'Patrimônio atualizado com sucesso.');
        }

        return back()->withInput()->with('error', 'Ocorreu um erro ao tentar atualizar o registro.');
    }

    public function destroy($id)
    {
        if ($this->model->findOrFail($id)->delete()) {
            return redirect()->route('patrimonies.index')->with('success', 'Patrimônio removido com sucesso.');
        }

        return back()->with('error', 'Patrimônio não foi removido.');
    }

    public function historic($patrimony_id)
    {
        $object = $this->model->findOrFail($patrimony_id);
        $contentHeader = 'Patrimônio';

        return view('patrimonies.historic', compact('object', 'contentHeader'));
    }

    public function saveHistoric(HistoricCreateRequest $request)
    {
        $historic = PatrimonyHistoric::create($request->all());

        if ($historic->id) {
            return redirect()->route('patrimonies.show', $historic->patrimony_id)->with('success', 'Histórico cadastrado com sucesso.');
        }

        return back();
    }
}
