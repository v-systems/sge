<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\Users\UserCreateRequest;
use App\Http\Requests\Users\UserDestroyRequest;
use App\Http\Requests\Users\UserUpdateRequest;
use App\Role;
use App\User;
use App\Enterprise;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    private $model;

    public function __construct(User $user)
    {
        $this->model = $user;
    }

    private function mountSelects()
    {
        $roles = Role::orderBy('label')->lists('label', 'id');
        $enterprises = Enterprise::orderBy('name')->lists('name', 'id');

        \View::share(compact('roles', 'enterprises'));
    }

    public function index()
    {
    	$users = $this->model->get();
    	return view('users.index', ['users' => $users]);
    }

    public function create()
    {
        $this->mountSelects();
    	return view('users.create');
    }

    public function store(UserCreateRequest $request)
    {
        $model = $this->model->create($request->all());

        if ($model->id) {
            return redirect()->route('users.index')->with('success', 'Usuário cadastrado com sucesso.');
        }

        return back()->withInput($request->except('password'));
    }

    public function edit($id)
    {
        $user = $this->model->findOrFail($id);
        $this->mountSelects();
        return view('users.edit', compact('user'));
    }

    public function update($id, UserUpdateRequest $request)
    {
        if ($this->model->findOrFail($id)->update($request->all()))
            return redirect()->route('users.index')->with('success', 'Usuário atualizado com sucesso.');

        return back()->withInput()->with('error', 'Ocorreu um erro ao tentar entrar no sistema');
    }

    public function destroy(UserDestroyRequest $request, $id)
    {
        if ($this->model->findOrFail($id)->delete()) {
            return redirect()->route('users.index')->with('success', 'Usuário removido com sucesso.');
        }

        return back()->with('error', 'Usuário não foi removido.');
    }
}
