<?php

namespace App\Http\Controllers;

use Auth;
use App\EquipmentType;
use App\IpEquipment;
use App\FixedIp;
use App\Local;
use App\Department;
use App\HostMv;

use App\Http\Requests\FixedIps\FixedIpsCreateRequest;
use App\Http\Requests\FixedIps\FixedIpsUpdateRequest;
use App\Http\Controllers\Controller;

class FixedIpsController extends Controller
{

    private $model;

    public function __construct(FixedIp $model)
    {
        parent::__construct();

        $this->model = $model;
    }

    private function mountSelects()
    {
        $equipmentTypes = EquipmentType::orderBy('name')->lists('name', 'id');
        $ipsEquipments = IpEquipment::orderBy('name')->lists('name', 'id');
        $locals = Local::orderBy('name')->lists('name', 'id');
        $departments = Department::orderBy('name')->lists('name', 'id');
        $hostMvs = HostMv::orderBy('name')->lists('name', 'id');

        \View::share(compact('ipsEquipments', 'locals', 'departments', 'hostMvs', 'equipmentTypes'));
    }

    public function index()
    {
        if (Auth::user()->role_id != 1) {
            $model = $this->model->select()->where('enterprise_id', Auth::user()->enterprise_id);
        } else {
            $model = $this->model;
        }

        return view('fixedips.index', ['fixedips' => $model->get()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->mountSelects();
        return view('fixedips.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FixedIpsCreateRequest $request)
    {
        $model = $this->model->create($request->all());

        if ($model->id) {
            return redirect()->route('fixedips.index')->with('success', 'Ip Fixo cadastrado com sucesso.');
        }

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $fixedip = $this->model->findOrFail($id);
        return view('fixedips.show', compact('fixedip'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $fixedip = $this->model->findOrFail($id);
        $this->mountSelects();
        return view('fixedips.edit', compact('fixedip'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(FixedIpsUpdateRequest $request, $id)
    {
        if ($this->model->findOrFail($id)->update($request->all()))
            return redirect()->route('fixedips.index')->with('success', 'Ip Fixo atualizado com sucesso.');

        return back()->withInput()->with('error', 'Ocorreu um erro ao tentar entrar no sistema');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ($this->model->findOrFail($id)->delete()) {
            return redirect()->route('fixedips.index')->with('success', 'Ip fixo removido com sucesso.');
        }

        return back()->with('error', 'Ip fixo não pode ser removido.');
    }
}
