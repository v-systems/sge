<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;

use App\Part;
use App\ChipNumber;
use App\FixedIp;
use App\User;

class HomeController extends Controller
{
    public function index()
    {
        $parts = Part::all()->count();
        $chipNumbers = ChipNumber::all()->count();
        $fixedIps = FixedIp::all()->count();
        $users = User::all()->count();

        return view('home') ->with('parts', $parts)
                            ->with('chipNumbers', $chipNumbers)
                            ->with('fixedIps', $fixedIps)
                            ->with('users', $users);
    }
}
