<?php

namespace App\Http\Controllers;

use App\Situation;

use App\Http\Requests\Situations\SituationCreateRequest;
use App\Http\Requests\Situations\SituationUpdateRequest;

use App\Http\Requests;

class SituationsController extends Controller
{
    private $model;

    public function __construct(Situation $situation)
    {
        $this->model = $situation;
    }

    public function index()
    {
        return view('situations.index', ['situations' => $this->model->get()]);
    }

    public function create()
    {
        return view('situations.create');
    }

    public function store(SituationCreateRequest $request)
    {
        $model = $this->model->create($request->all());

        if ($model->id) {
            return redirect()->route('situations.index')->with('success', 'Situação cadastrada com sucesso.');
        }

        return back();
    }

    public function edit($id)
    {
        $situation = $this->model->findOrFail($id);
        return view('situations.edit', compact('situation'));
    }

    public function show($id)
    {
        $situation = $this->model->findOrFail($id);

        return view('situations.show', compact('situation'));
    }

    public function update(SituationUpdateRequest $request, $id)
    {
        if ($this->model->findOrFail($id)->update($request->all())) {
            return redirect()->route('situations.index')->with('success', 'Situação atualizada com sucesso.');
        }

        return back()->withInput()->with('error', 'Ocorreu um erro ao tentar atualizar o registro.');
    }

    public function destroy($id)
    {
        if ($this->model->findOrFail($id)->delete()) {
            return redirect()->route('situations.index')->with('success', 'Situação removida com sucesso.');
        }

        return back()->with('error', 'Situação não foi removida.');
    }
}
