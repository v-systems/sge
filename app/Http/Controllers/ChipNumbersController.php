<?php

namespace App\Http\Controllers;

use Auth;
use App\ChipNumber;
use App\CellphonePlan;
use App\Job;
use App\LineSituation;

use Illuminate\Http\Request;
use App\Http\Requests\ChipNumbers\ChipNumbersCreateRequest;
use App\Http\Requests\ChipNumbers\ChipNumbersUpdateRequest;
use App\Http\Controllers\Controller;

class ChipNumbersController extends Controller
{
    private $model;

    public function __construct(ChipNumber $model)
    {
        parent::__construct();

        $this->model = $model;
    }

    private function mountSelects()
    {
        $cellphonePlans = CellphonePlan::orderBy('name')->lists('name', 'id');
        $lineSituations = LineSituation::orderBy('name')->lists('name', 'id');
        $jobs = Job::orderBy('name')->lists('name', 'id');

        \View::share(compact('cellphonePlans', 'lineSituations', 'jobs'));
    }

    public function index()
    {
        if (Auth::user()->role_id != 1) {
            $model = $this->model->select()->where('enterprise_id', Auth::user()->enterprise_id);
        } else {
            $model = $this->model;
        }

        return view('chipNumbers.index', ['chipNumbers' => $model->get()]);
    }

    public function create()
    {
        $this->mountSelects();
        return view('chipNumbers.create');
    }

    public function store(ChipNumbersCreateRequest $request)
    {
        $model = $this->model->create($request->all());

        if ($model->id) {
            return redirect()->route('chipNumbers.index')->with('success', 'Número e Chip cadastrado com sucesso.');
        }

        return back();
    }

    public function show($id)
    {
        $chipNumber = $this->model->findOrFail($id);

        return view('chipNumbers.show', compact('chipNumber'));
    }

    public function edit($id)
    {
        $chipNumber = $this->model->findOrFail($id);
        $this->mountSelects();
        return view('chipNumbers.edit', compact('chipNumber'));
    }

    public function update(ChipNumbersUpdateRequest $request, $id)
    {
        if ($this->model->findOrFail($id)->update($request->all()))
            return redirect()->route('chipNumbers.index')->with('success', 'Número e Chip atualizado com sucesso.');

        return back()->withInput()->with('error', 'Ocorreu um erro ao tentar atualizar o registro.');
    }

    public function destroy($id)
    {
        if ($this->model->findOrFail($id)->delete()) {
            return redirect()->route('chipNumbers.index')->with('success', 'Número e Chip removido com sucesso.');
        }

        return back()->with('error', 'Número e Chip não foi removido.');
    }
}
