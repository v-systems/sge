<?php

namespace App\Http\Controllers;

use App\Department;

use App\Http\Requests\Departments\DepartmentCreateRequest;
use App\Http\Requests\Departments\DepartmentUpdateRequest;

use App\Http\Requests;

class DepartmentsController extends Controller
{
    private $model;

    public function __construct(Department $department)
    {
        $this->model = $department;
    }

    public function index()
    {
        return view('departments.index', ['departments' => $this->model->get()]);
    }

    public function create()
    {
        return view('departments.create');
    }

    public function store(DepartmentCreateRequest $request)
    {
        $model = $this->model->create($request->all());

        if ($model->id) {
            return redirect()->route('departments.index')->with('success', 'Departamento cadastrado com sucesso.');
        }

        return back();
    }

    public function edit($id)
    {
        $department = $this->model->findOrFail($id);
        return view('departments.edit', compact('department'));
    }

    public function show($id)
    {
        $department = $this->model->findOrFail($id);

        return view('departments.show', compact('department'));
    }

    public function update(DepartmentUpdateRequest $request, $id)
    {
        if ($this->model->findOrFail($id)->update($request->all()))
            return redirect()->route('departments.index')->with('success', 'Departamento atualizado com sucesso.');

        return back()->withInput()->with('error', 'Ocorreu um erro ao tentar atualizar o registro.');
    }

    public function destroy($id)
    {
        if ($this->model->findOrFail($id)->delete()) {
            return redirect()->route('departments.index')->with('success', 'Departamento removido com sucesso.');
        }

        return back()->with('error', 'Departamento não foi removida.');
    }
}
