<?php

namespace App\Http\Controllers;

use App\Equipment;

use App\Http\Requests\Equipments\EquipmentCreateRequest;
use App\Http\Requests\Equipments\EquipmentUpdateRequest;

use App\Http\Requests;

class EquipmentsController extends Controller
{
    private $model;

    public function __construct(Equipment $equipment)
    {
        $this->model = $equipment;
    }

    public function index()
    {
        return view('equipments.index', ['equipments' => $this->model->get()]);
    }

    public function create()
    {
        return view('equipments.create');
    }

    public function store(EquipmentCreateRequest $request)
    {
        $model = $this->model->create($request->all());

        if ($model->id) {
            return redirect()->route('equipments.index')->with('success', 'Equipamento cadastrado com sucesso.');
        }

        return back();
    }

    public function edit($id)
    {
        $equipment = $this->model->findOrFail($id);
        return view('equipments.edit', compact('equipment'));
    }

    public function show($id)
    {
        $equipment = $this->model->findOrFail($id);

        return view('equipments.show', compact('equipment'));
    }

    public function update(EquipmentUpdateRequest $request, $id)
    {
        if ($this->model->findOrFail($id)->update($request->all()))
            return redirect()->route('equipments.index')->with('success', 'Equipamento atualizado com sucesso.');

        return back()->withInput()->with('error', 'Ocorreu um erro ao tentar atualizar o registro.');
    }

    public function destroy($id)
    {
        if ($this->model->findOrFail($id)->delete()) {
            return redirect()->route('equipments.index')->with('success', 'Equipamento removido com sucesso.');
        }

        return back()->with('error', 'Equipamento não foi removida.');
    }
}
