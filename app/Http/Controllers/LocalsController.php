<?php

namespace App\Http\Controllers;

use App\Local;

use App\Http\Requests\Locals\LocalCreateRequest;
use App\Http\Requests\Locals\LocalUpdateRequest;

use App\Http\Requests;

class LocalsController extends Controller
{
    private $model;

    public function __construct(Local $local)
    {
        $this->model = $local;
    }

    public function index()
    {
        return view('locals.index', ['locals' => $this->model->get()]);
    }

    public function create()
    {
        return view('locals.create');
    }

    public function store(LocalCreateRequest $request)
    {
        $model = $this->model->create($request->all());

        if ($model->id) {
            return redirect()->route('locals.index')->with('success', 'Local Instalado cadastrado com sucesso.');
        }

        return back();
    }

    public function edit($id)
    {
        $local = $this->model->findOrFail($id);
        return view('locals.edit', compact('local'));
    }

    public function show($id)
    {
        $local = $this->model->findOrFail($id);

        return view('locals.show', compact('local'));
    }

    public function update(LocalUpdateRequest $request, $id)
    {
        if ($this->model->findOrFail($id)->update($request->all()))
            return redirect()->route('locals.index')->with('success', 'Local Instalado atualizado com sucesso.');

        return back()->withInput()->with('error', 'Ocorreu um erro ao tentar atualizar o registro.');
    }

    public function destroy($id)
    {
        if ($this->model->findOrFail($id)->delete()) {
            return redirect()->route('locals.index')->with('success', 'Local Instalado removido com sucesso.');
        }

        return back()->with('error', 'Local Instalado não foi removida.');
    }
}
