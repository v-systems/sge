<?php

namespace App\Http\Controllers;

use App\Http\Requests\Roles\RoleRequest;
use App\Permission;
use App\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RolesController extends Controller
{
    private $model;

    public function __construct(Role $role)
    {
        $this->model = $role;
    }

    private function mountSelects()
    {
        $permissions = Permission::getList();

        \View::share(compact('permissions'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = $this->model->get();
        return view('roles.index', ['roles' => $roles]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->mountSelects();
        return view('roles.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RoleRequest $request)
    {
        DB::beginTransaction();
        $model = $this->model->create($request->all());

        if ($model->id) {
            if ($model->givePermissionArray($request->get('permissions'))) {
                DB::commit();
                return redirect()->route('roles.index')->with('success', 'Perfil cadastrado com sucesso.');
            }
        }

        DB::rollBack();
        return back()->withInput();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = $this->model->findOrFail($id);
        $this->mountSelects();
        return view('roles.edit', compact('role'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(RoleRequest $request, $id)
    {
        DB::beginTransaction();
        $model = $this->model->findOrFail($id);
        if ($model->update($request->all())){
            if ($model->givePermissionArray($request->get('permissions'))) {
                DB::commit();
                return redirect()->route('roles.index')->with('success', 'Perfil atualizado com sucesso.');
            }
        }

        DB::rollBack();
        return back()->withInput()->with('error', 'Ocorreu um erro ao tentar entrar no sistema');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ($this->model->findOrFail($id)->delete()) {
            return redirect()->route('roles.index')->with('success', 'Perfil removido com sucesso.');
        }

        return back()->with('error', 'Perfil não foi removido.');
    }
}
