<?php

namespace App\Http\Controllers;

use Auth;
use App\Part;
use App\Equipment;
use App\Local;
use App\Department;
use App\Computer;
use App\Situation;
use Illuminate\Http\Request;

use App\Http\Requests\Parts\PartCreateRequest;
use App\Http\Requests\Parts\PartUpdateRequest;
use App\Http\Controllers\Controller;

class PartsController extends Controller
{
    private $model;

    public function __construct(Part $model)
    {
        parent::__construct();

        $this->model = $model;
    }

    private function mountSelects()
    {
        $equipments = Equipment::orderBy('name')->lists('name', 'id');
        $locals = Local::orderBy('name')->lists('name', 'id');
        $departments = Department::orderBy('name')->lists('name', 'id');
        $computers = Computer::orderBy('name')->lists('name', 'id');
        $situations = Situation::orderBy('name')->lists('name', 'id');

        \View::share(compact('equipments', 'locals', 'departments', 'situations', 'computers'));
    }

    public function index()
    {
        if (Auth::user()->role_id != 1) {
            $model = $this->model->select()->where('enterprise_id', Auth::user()->enterprise_id);
        } else {
            $model = $this->model;
        }

        return view('parts.index', ['parts' => $model->get()]);
    }

    public function create()
    {
        $this->mountSelects();
        return view('parts.create');
    }

    public function store(PartCreateRequest $request)
    {
        $model = Part::create($request->all());

        if ($model->id) {
            return redirect()->route('parts.index')->with('success', 'Peça cadastrada com sucesso.');
        }

        return back();
    }

    public function show($id)
    {
        $part = $this->model->findOrFail($id);
        return view('parts.show', compact('part'));
    }

    public function edit($id)
    {
        $part = $this->model->findOrFail($id);
        $this->mountSelects();
        return view('parts.edit', compact('part'));
    }

    public function update(PartUpdateRequest $request, $id)
    {
        if ($this->model->findOrFail($id)->update($request->all()))
            return redirect()->route('parts.index')->with('success', 'Peça atualizada com sucesso.');

        return back()->withInput()->with('error', 'Ocorreu um erro ao tentar entrar no sistema');
    }

    public function destroy($id)
    {
        if ($this->model->findOrFail($id)->delete()) {
            return redirect()->route('parts.index')->with('success', 'Peça removida com sucesso.');
        }

        return back()->with('error', 'Peça não foi removida.');
    }
}
