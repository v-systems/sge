<?php

namespace App\Http\Controllers;

use App\Enterprise;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use View;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
        $enterprises = Enterprise::orderBy('name')->lists('name','id')->toArray();

        View::share('enterprises', $enterprises);
    }
}
