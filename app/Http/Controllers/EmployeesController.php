<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Employee;
use App\Department;
use App\Http\Requests\Employees\EmployeeCreateRequest;
use App\Http\Requests\Employees\EmployeeUpdateRequest;

class EmployeesController extends Controller
{

    private $model;

    public function __construct(Employee $model)
    {
        parent::__construct();

        $this->model = $model;
    }

    private function mountSelects()
    {
        $departments = Department::orderBy('name')->lists('name', 'id');
        \View::share(compact('departments'));
    }

    public function index()
    {
        return view('employees.index', ['employees' => $this->model->get()]);
    }

    public function create()
    {
        $this->mountSelects();
        return view('employees.create');
    }

    public function store(EmployeeCreateRequest $request)
    {
        $model = $this->model->create($request->all());

        if ($model->id) {
            return redirect()->route('employees.index')->with('success', 'Funcionário cadastrado com sucesso.');
        }

        return back();
    }

    public function edit($id)
    {
        $this->mountSelects();
        $employee = $this->model->findOrFail($id);
        return view('employees.edit', compact('employee'));
    }

    public function show($id)
    {
        $employee = $this->model->findOrFail($id);

        return view('employees.show', compact('employee'));
    }

    public function update(employeeUpdateRequest $request, $id)
    {
        if ($this->model->findOrFail($id)->update($request->all())) {
            return redirect()->route('employees.index')->with('success', 'Funcionário atualizado com sucesso.');
        }

        return back()->withInput()->with('error', 'Ocorreu um erro ao tentar atualizar o registro.');
    }

    public function destroy($id)
    {
        if ($this->model->findOrFail($id)->delete()) {
            return redirect()->route('employees.index')->with('success', 'Funcionário removido com sucesso.');
        }

        return back()->with('error', 'Funcionário não foi removido.');
    }
}
