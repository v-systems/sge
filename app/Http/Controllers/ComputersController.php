<?php

namespace App\Http\Controllers;

use App\Computer;

use App\Http\Requests\Computers\ComputerCreateRequest;
use App\Http\Requests\Computers\ComputerUpdateRequest;

use App\Http\Requests;

class ComputersController extends Controller
{
    private $model;

    public function __construct(Computer $computer)
    {
        $this->model = $computer;
    }

    public function index()
    {
        return view('computers.index', ['computers' => $this->model->get()]);
    }

    public function create()
    {
        return view('computers.create');
    }

    public function store(ComputerCreateRequest $request)
    {
        $model = $this->model->create($request->all());

        if ($model->id) {
            return redirect()->route('computers.index')->with('success', 'Computador Instalado cadastrado com sucesso.');
        }

        return back();
    }

    public function edit($id)
    {
        $computer = $this->model->findOrFail($id);
        return view('computers.edit', compact('computer'));
    }

    public function show($id)
    {
        $computer = $this->model->findOrFail($id);

        return view('computers.show', compact('computer'));
    }

    public function update(ComputerUpdateRequest $request, $id)
    {
        if ($this->model->findOrFail($id)->update($request->all()))
            return redirect()->route('computers.index')->with('success', 'Computador Instalado atualizado com sucesso.');

        return back()->withInput()->with('error', 'Ocorreu um erro ao tentar atualizar o registro.');
    }

    public function destroy($id)
    {
        if ($this->model->findOrFail($id)->delete()) {
            return redirect()->route('computers.index')->with('success', 'Computador Instalado removido com sucesso.');
        }

        return back()->with('error', 'Computador Instalado não foi removida.');
    }
}
