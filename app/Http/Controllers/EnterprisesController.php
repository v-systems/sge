<?php

namespace App\Http\Controllers;

use App\Enterprise;

use App\Http\Requests\Enterprises\EnterprisesCreateRequest;
use App\Http\Requests\Enterprises\EnterprisesUpdateRequest;

use App\Http\Requests;

class EnterprisesController extends Controller
{
    private $model;

    public function __construct(Enterprise $enterprise)
    {
        $this->model = $enterprise;
    }

    public function index()
    {
        return view('enterprises.index', ['enterprises' => $this->model->get()]);
    }

    public function create()
    {
        return view('enterprises.create');
    }

    public function store(EnterprisesCreateRequest $request)
    {
        $model = $this->model->create($request->all());

        if ($model->id) {
            return redirect()->route('enterprises.index')->with('success', 'Empresa cadastrada com sucesso.');
        }

        return back();
    }

    public function edit($id)
    {
        $enterprise = $this->model->findOrFail($id);
        return view('enterprises.edit', compact('enterprise'));
    }

    public function update(EnterprisesUpdateRequest $request, $id)
    {
        if ($this->model->findOrFail($id)->update($request->all()))
            return redirect()->route('enterprises.index')->with('success', 'Número e Chip atualizado com sucesso.');

        return back()->withInput()->with('error', 'Ocorreu um erro ao tentar atualizar o registro.');
    }

    public function destroy($id)
    {
        if ($this->model->findOrFail($id)->delete()) {
            return redirect()->route('enterprises.index')->with('success', 'Empresa removida com sucesso.');
        }

        return back()->with('error', 'Empresa não foi removida.');
    }
}
