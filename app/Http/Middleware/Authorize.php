<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Gate;

class Authorize
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $permission = str_replace($request->route()->getAction()['namespace'] . '\\', '', $request->route()->getAction()['controller']);

        if (Gate::denies($permission) && strstr($permission, 'saveHistoric') === false ) {
            return abort(403, 'You dont have Permission.');
        }

        return $next($request);
    }
}
