<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Part extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'code',
        'equipment_id',
        'description',
        'serial_number',
        'imei',
        'buy_date',
        'invoice_number',
        'supplier',
        'local_id',
        'department_id',
        'computer_id',
        'situation_id',
        'installation_date',
        'warranty',
        'historic',
        'enterprise_id'
    ];

    protected $dates = [
        'buy_date',
        'installation_date',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function setBuyDateAttribute($value)
    {
        $this->attributes['buy_date'] = date('Y-m-d', strtotime(str_replace('/','-',$value)));
    }

    public function setInstallationDateAttribute($value)
    {
        $this->attributes['installation_date'] = date('Y-m-d', strtotime(str_replace('/','-',$value)));
    }

    public function enterprise()
    {
        return $this->belongsTo('App\Enterprise');
    }

    public function equipment()
    {
        return $this->belongsTo('App\Equipment');
    }

    public function local()
    {
        return $this->belongsTo('App\Local');
    }

    public function department()
    {
        return $this->belongsTo('App\Department');
    }

    public function computer()
    {
        return $this->belongsTo('App\Computer');
    }

    public function situation()
    {
        return $this->belongsTo('App\Situation');
    }
}
