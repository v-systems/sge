<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Patrimony extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'enterprise_id',
        'type_property_id',
        'situation_id',
        'local_id',
        'department_responsible_id',
        'employee_id',
        'department_use_id',
        'code',
        'description',
        'nf',
        'supplier',
        'value',
        'nf_date',
        'release_date'
    ];

    protected $dates = [
        'nf_date',
        'release_date',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function addHistoric($request, $model)
    {
        if (!empty($request->get('historic'))) {
            $historic = new PatrimonyHistoric();
            $historic->historic = $request->get('historic');
            $historic->user_id = auth()->user()->id;

            $model->historics()->save($historic);
        }
    }

    public function setNfDateAttribute($value)
    {
        $this->attributes['nf_date'] = date('Y-m-d', strtotime(str_replace('/','-',$value)));
    }

    public function setReleaseDateAttribute($value)
    {
        $this->attributes['release_date'] = date('Y-m-d', strtotime(str_replace('/','-',$value)));
    }

    public function setValueAttribute($value)
    {
        $this->attributes['value'] = str_replace(['.', ','], ['', '.'], $value);
    }

    public function getValueAttribute($value)
    {
        return number_format($value, 2, ',', '.');
    }

    public function enterprise()
    {
        return $this->belongsTo('App\Enterprise');
    }

    public function local()
    {
        return $this->belongsTo('App\Local');
    }

    public function departmentResponsible()
    {
        return $this->belongsTo('App\Department', 'department_responsible_id');
    }

    public function departmentUSe()
    {
        return $this->belongsTo('App\Department', 'department_use_id');
    }

    public function situation()
    {
        return $this->belongsTo('App\Situation');
    }

    public function typeProperty()
    {
        return $this->belongsTo('App\TypeProperty');
    }

    public function employee()
    {
        return $this->belongsTo('App\Employee');
    }

    public function historics()
    {
        return $this->hasMany('App\PatrimonyHistoric');
    }
}
