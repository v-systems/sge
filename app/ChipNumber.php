<?php

namespace App;

use Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class ChipNumber extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'enterprise_id',
        'number',
        'chip_number',
        'cellphone_plan_id',
        'data',
        'buy_date',
        'invoice_number',
        'employee',
        'job_id',
        'possession_date',
        'line_situation_id',
        'historic'
    ];

    protected $dates = [
        'buy_date',
        'possession_date',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function get()
    {
        if (Auth::user()->role_id == 1) return parent::get();
        return $this->where('enterprise_id', Auth::user()->enterprise_id)->get();
    }

    public function setEnterpriseIdAttribute($value)
    {
        if (Auth::user()->role_id != 1) $value = Auth::user()->enterprise_id;
        $this->attributes['enterprise_id'] = $value;
    }

    public function enterprise()
    {
        return $this->belongsTo('App\Enterprise');
    }

    public function setBuyDateAttribute($value)
    {
        $this->attributes['buy_date'] = date('Y-m-d', strtotime(str_replace('/','-',$value)));
    }

    public function setPossessionDateAttribute($value)
    {
        $this->attributes['possession_date'] = date('Y-m-d', strtotime(str_replace('/','-',$value)));
    }

    public function cellphonePlan()
    {
        return $this->belongsTo('App\CellphonePlan');
    }

    public function lineSituation()
    {
        return $this->belongsTo('App\LineSituation');
    }

    public function job()
    {
        return $this->belongsTo('App\Job');
    }
}
