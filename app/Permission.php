<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{

    protected $fillable = ['name', 'label', 'controller'];

    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    public static function getList()
    {
        $permissions = self::orderBy('controller')->get();

        $permissionsList = [];

        foreach ($permissions as $permission) {
            $permissionsList[$permission->controller][$permission->id] = $permission->label;
        }

        return $permissionsList;
    }
}
