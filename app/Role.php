<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $fillable = [
        'name', 'label'
    ];

    public function permissions()
    {
        return $this->belongsToMany(Permission::class);
    }

    public function givePermission(Permission $permission)
    {
        return $this->permissions()->save($permission);
    }

    public function givePermissionArray($permission)
    {
        return $this->permissions()->sync($permission);
    }
}
