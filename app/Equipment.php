<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Equipment extends Model
{

    protected $table = "equipments";

    protected $fillable = [
    	'name'
    ];

    protected $dates = [
    	'created_at',
    	'updated_at'
    ];

    public function parts()
    {
    	return $this->belongsTo('App\Part');
    }
}
