<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class IpEquipment extends Model
{

    protected $fillable = [
    	'name',
    	'description'
    ];

    protected $dates = [
    	'created_at',
    	'updated_at',
    	'deleted_at'
    ];

    public function fixedIps()
    {
        return $this->belongsTo('App\FixedIp');
    }
}
