<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $fillable = [
        'enterprise_id',
        'department_id',
        'name'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function enterprise()
    {
        return $this->belongsTo('App\Enterprise');
    }

    public function department()
    {
        return $this->belongsTo('App\Department');
    }
}
